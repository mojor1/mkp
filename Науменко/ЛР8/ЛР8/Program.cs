﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.IO;

namespace ЛР8
{
    class Program
    {
        static void Main(string[] args)
        {
            ЗД1();
            ЗД2();

        }

        private static void ЗД2()
        {
            string text1 = Console.ReadLine();
            string text = File.ReadAllText(text1);
            string pattern = @"sm";
            Regex reg = new Regex(pattern);
            string[] mas = text.Split('\n');
            for (int i = 0; i < mas.Length; i++)
            {
                if (reg.IsMatch(mas[i]))
                {
                    string size = reg.Match(mas[i]).Groups[1].Value;
                    mas[i] = reg.Replace(mas[i], @"<SMALL>");
                    mas[i] = mas[i] + @"</SMALL>";
                }
            }
            pattern = @"h\d";
            reg = new Regex(pattern);
            for (int i = 0; i < mas.Length; i++)
            {
                if (reg.IsMatch(mas[i]))
                {
                    string size = reg.Match(mas[i]).Groups[1].Value;
                    mas[i] = reg.Replace(mas[i], @"<" + reg.Match(mas[i]).Value + @">");
                    mas[i] = mas[i] + @"</" + reg.Match(mas[i]).Value + @">";
                }
            }
            text = string.Join("<br>", mas, 0, mas.Length);
            GreateHtmlDoc(text);
        }

        public static void GreateHtmlDoc(string TextInBody)
        {
            StringBuilder sb = new StringBuilder();
            sb.Append("<html >");
            sb.Append("<head >");
            string meta = @"<meta charset=""UTF-8"">";
            sb.Append(meta);
            sb.Append("<title >");
            sb.Append("</title >");
            sb.Append("</head >");
            sb.Append("<body >");
            sb.Append(TextInBody);
            sb.Append("</body >");
            sb.Append("</html >");
            using (StreamWriter sw = new StreamWriter("MyHtml.html"))
            {
                sw.Write(sb.ToString());
                sw.Close();
                Console.WriteLine("Файл создан успешно!");
                System.Diagnostics.Process.Start("MyHtml.html");
            }
        }
        private static void ЗД1()
        {
            string text1 = Console.ReadLine();
            string text = File.ReadAllText(text1);
            string[] mas = text.Split();
            Regex regex = new Regex(@"\d+");
            foreach (var it in mas)
            {
                if (regex.IsMatch(it))
                    Console.WriteLine(it);
            }
        }
    }
}
