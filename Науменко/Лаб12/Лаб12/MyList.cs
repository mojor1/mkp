﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Лаб12
{
    public class MyList
    {
         List<int> mylist = new List<int>();

        public MyList()
        {
        }

        public MyList(int[] vs)
        {
            if (vs.Length <= 10)
                this.mylist = vs.ToList();
            else throw new MyExeption("Последовательность переполнена!");
        }

        public MyList(List<int> mylist)
        {
            if (mylist.Count <= 10)
                this.mylist = mylist;
            else throw new MyExeption("Последовательность переполнена!");
        }
        public MyList(int el)
        {
            mylist.Add(el);
        }
        public void Add(int el)
        {
            if (mylist.Count < 10)
                mylist.Add(el);
            else throw new MyExeption("Последовательность переполнена!");
        }
public int Sum()
        {
            int sum = 0;
            foreach(var it in mylist)
            {
                sum += it;
            }
            return sum;
        } 
       public int GetEl(int i)
        {
            if (i < mylist.Count)
            {
                return mylist.ToArray()[i];
            }
            else throw new MyExeption("Такого элемента нет!");
        }
        public override string ToString()
        {
            string st = "";
            foreach (var it in mylist)
            {
                st += it.ToString()+" ";
            }
            return st;
        }
        public void Dell(int nom)
        {          
            if(nom<mylist.Count)
            mylist.RemoveAt(nom);
            else throw new MyExeption("Такого элемента нет!");
        }
        public void DellZnach(int nom)
        {
           bool flag= mylist.Remove(nom);
            if(!flag)
            throw new MyExeption("Такого элемента нет!");
        }
    }
}
