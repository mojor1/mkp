﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Доп_задание
{
    class InOutArray
    {
        static double[] GetArray(string line)
        {
            string[] newline = line.Split();
            double[] mas = new double[newline.Length];
            for (int i = 0; i < mas.Length; i++)
                mas[i] = Convert.ToDouble(newline[i]);
            return mas;
        }
        static void PrintArray(double[] array)
        {
                Console.WriteLine("Ваш массив:");
                foreach (var it in array)
                    Console.WriteLine(it);
        }
static void PrintArray(double[] array, string name, int countColumn)
        {
            Console.WriteLine("Массив "+name);
            for(int i=0; i<array.Length; i++)
            {
                if((i+1)%countColumn==0)
                    Console.WriteLine($"{name}[{i}]={array[i]}");
                else Console.Write($"{name}[{i}]={array[i]}   ");
            }
        }
    }
}
