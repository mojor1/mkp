﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР6
{
    class Array
    {
        public Month month = new Month();
        public static int[] GetArrayI(int kol)
        {
            int[] mas = new int[kol];
            string a;
            while (true)
            {
                Console.WriteLine("Введите Р если хотите вводить значения руками. Введите букву Г если хотите сгенирировать случайный массив");
                a = Console.ReadLine();
                if (a == "Р")
                {
                    mas = Array.YouArray(mas);
                    break;
                }
                else
                    if (a == "Г")
                {
                    mas = Array.RandomArray(mas);
                    break;
                }
                else Console.WriteLine("Неправельно введён символ");
            }
            return mas;
        }
        public static double[] GetArrayD(int kol)
        {
            double[] mas = new double[kol];
            string a;
            while (true)
            {
                Console.WriteLine("Введите Р если хотите вводить значения руками. Введите букву Г если хотите сгенирировать случайный массив");
                a = Console.ReadLine();
                if (a == "Р")
                {
                    mas = Array.YouArray(mas);
                    break;
                }
                else
                    if (a == "Г")
                {
                    mas = Array.RandomArray(mas);
                    break;
                }
                else Console.WriteLine("Неправельно введён символ");
            }
            return mas;
        }
        public static int[] RandomArray(int[] mas)
        {
            Random random = new Random();
            for (int i = 0; i < mas.Length; i++)
                mas[i] = random.Next();
            Array.Vivod(mas);
            return mas;
        }
        public static double[] RandomArray(double[] mas)
        {
            Random random = new Random();
            for (int i = 0; i < mas.Length; i++)
                mas[i] = random.NextDouble();
            Array.Vivod(mas);
            return mas;
        }
        public static void Vivod(int[] mas)
        {
            Console.WriteLine("Ваш массив:");
            foreach (var it in mas)
                Console.WriteLine(it);
        }
        public static void Vivod(double[] mas)
        {
            Console.WriteLine("Ваш массив:");
            foreach (var it in mas)
                Console.WriteLine(it);
        }
        public static int[] YouArray(int[] mas)
        {
            Console.WriteLine("Введите элементы массива");
            for (int i = 0; i < mas.Length; i++)
            {
                Console.WriteLine($"Введите {i+1} элемент массива");
                mas[i] = int.Parse(Console.ReadLine());
            }
            return mas;
        }
      public  static double[] YouArray(double[] mas)
        {
            Console.WriteLine("Введите элементы массива");
            for (int i = 0; i < mas.Length; i++)
            {
                Console.WriteLine($"Введите {i + 1} элемент массива");
                mas[i] = double.Parse(Console.ReadLine());
            }
            return mas;
        }
        public double[] GetArray(string month)
        {
            double[] mas=new double[31];
            for (int i = 0; i < this.month.month.Length; i++)
                if (this.month.month[i] == month)
                {
                    mas = new double[this.month.days[i]];
                    string a;
                    while (true) {
                        Console.WriteLine("Введите Р если хотите вводить значения руками. Введите букву Г если хотите сгенирировать случайный массив");
                        a = Console.ReadLine();
                        if (a == "Р")
                        {
                            mas = Array.YouArray(mas);
                            break;
                        }
                        else
                            if (a == "Г") { 
                            mas = Array.RandomArray(mas);
                        break;
                    }
                        else Console.WriteLine("Неправельно введён символ");
                            }
                    break;
                }
            return mas;
        }
        public int Kol(double[] mas)
        {
            int kol = 0;
            foreach (double it in mas)
                if (it < 0)
                    kol++;
            return kol;
        }
        public int Number(double y)
        {
            Console.WriteLine("Введите количество элементов");
           double[] mas=Array.GetArrayD(int.Parse(Console.ReadLine()));
            int number = 0;
            for (int i = 0; i < mas.Length; i++)
                if (i + 1 != mas.Length && mas[i] < y && mas[i + 1] > y)
                    number = i;
            return number;
        }
        public bool Proverka(double[] mas)
        {
            bool flag = true;
            double a = 0;
            int i = 0;
            foreach(var it in mas)
            {
                if (i == 0)
                    a = it;
                else if (a < it)
                    flag = false;
                i++;
            }
            if (flag)
                return true;
            else return false;
        }
    }
}
