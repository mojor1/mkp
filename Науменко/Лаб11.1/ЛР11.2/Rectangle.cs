﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР11._2
{
    class Rectangle : IComparable<Rectangle>
    {
        int a;
        int b;
        public int ploshad;
       public int A { get { return a; }  set
            {
                if (value > 0)
                    a = value;
            }
        }
        public int B
        {
            get { return b; }
            set
            {
                if (value > 0)
                    b = value;
            }
        }
        public Rectangle()
        {
        }

        public Rectangle(int a, int b)
        {
            this.A = a;
            this.B = b;
            SetPloshad();
        }
        private void SetPloshad()
        {
            ploshad = a * b;
        }
      
        public override string ToString()
        {
            return $"стороны прямоугольника - {a}, {b}, площадь: {ploshad}";
        }

        public int CompareTo(Rectangle other)
        {
            return ploshad - other.ploshad;
        }
    }
}
