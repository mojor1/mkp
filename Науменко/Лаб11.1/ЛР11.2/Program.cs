﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР11._2
{
    class Program
    {
        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("Введите количество эллементов массива");
                int a = int.Parse(Console.ReadLine());
                Rectangle[] rectangles = new Rectangle[a];
                for (int i = 0; i < a; i++)
                {
                    Console.WriteLine("Введите ширену и длинну треугольника через энтер");
                    rectangles[i] = new Rectangle(int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()));
                }
                Array.Sort(rectangles);
            foreach (var it in rectangles)
                Console.WriteLine(it.ToString());
            }
            catch
            {
                Console.WriteLine("Вы что-то сделали не так");
            }
        }
    }
}
