﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Лаб11._1
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Ведите размерность масива");
            int a = int.Parse(Console.ReadLine());
            double[] vs = new double[100];
            bool flag = false;
            while (!flag)
            {
                Console.WriteLine("Введите 1 если хотите случайно сгенерировать массив, введите 2 если хотите ввести массив с клавиатуры");
                string b = Console.ReadLine();
                if (b == "1")
                {
                    vs = Generate(a);
                    foreach(double it in vs)
                        Console.WriteLine(it);
                    flag = true;
                }
                else if (b == "2") { 
                    vs = Inicialozate(a);
                    flag = true;
                }
                else Console.WriteLine("Неправильно введины данные");
            }
           Array.Sort(vs, new MasComparor());
            Console.WriteLine("---------------");
            foreach (double it in vs)
                Console.WriteLine(it);
        }

        private static double[] Inicialozate(int a)
        {
            double[] vs = new double[a];
            for (int i = 0; i < a; i++)
                vs[i] = double.Parse(Console.ReadLine());
            return vs;
        }

        private static double[] Generate(int a)
        {
            double[] vs = new double[a];
            Random random = new Random();
            for (int i = 0; i < a; i++)
                if (i % 2 == 1)
                    vs[i] = random.NextDouble();
                else vs[i] = random.Next(1, 10);
            return vs;
        }
    }
}
