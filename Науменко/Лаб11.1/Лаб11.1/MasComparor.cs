﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Лаб11._1
{
    class MasComparor : IComparer<double>
    {
        int IComparer<double>.Compare(double x, double y)
        {
            if (int.TryParse(x.ToString(), out int a) == false && int.TryParse(y.ToString(), out int b) == false)
                return (int)Math.Floor(x - y);
            else if (int.TryParse(x.ToString(), out int c)==true && int.TryParse(y.ToString(), out int d) == false)
                return -1;
            else if (int.TryParse(x.ToString(), out int e) == false && int.TryParse(y.ToString(), out int f)==true)
                return 1;
            else return (int)(x - y);
        }
    }
}
