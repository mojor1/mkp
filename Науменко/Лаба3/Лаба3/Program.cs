﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Лаба3
{
    class Program
    {
        static void Main(string[] args)
        {
            KolDay();
            int z;
            double y;
            double x = Primer();
            Zadacha(out z, out y, out x);
        }

        private static void Zadacha(out int z, out double y, out double x)
        {
            Console.WriteLine("ВВедите целые А и В");
            int A = int.Parse(Console.ReadLine());
            int B = int.Parse(Console.ReadLine());
            Console.WriteLine("ВВедите целые x, y, z");
            x = int.Parse(Console.ReadLine());
            y = int.Parse(Console.ReadLine());
            z = int.Parse(Console.ReadLine());
            if (A * B > x * y || A * B > x * z || A * B > y * z)
                Console.WriteLine("Пройдёт");
            else Console.WriteLine("Не пройдёт");
        }

        private static double Primer()
        {
            Console.WriteLine("Введите а и в");
            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());
            Console.WriteLine("Введите целый z");
            int z = int.Parse(Console.ReadLine());
            double x;
            if (z < a * b)
            {
                x = Math.Sqrt((Math.Pow(a, 2) + Math.Pow(a, 2)) * z);
            }
            else
                x = Math.Sin(Math.Pow(z, 2)) + Math.Abs(a * b * z);
            double y = (a * x + b * x * Math.Cos(Math.Sqrt(x)));
            Console.WriteLine("Ответ - " + y);
            return x;
        }

        private static void KolDay()
        {
            Console.WriteLine("Введите номер месяца");
            string c = Console.ReadLine();
            switch (c)
            {
                case "1":
                    Console.WriteLine("Январь - 31 день");
                    break;
                case "2":
                    Console.WriteLine("Февраль - 28 день");
                    break;
                case "3":
                    Console.WriteLine("Март - 31 день");
                    break;
                case "4":
                    Console.WriteLine("Апрель - 30 день");
                    break;
                case "5":
                    Console.WriteLine("Май - 31 день");
                    break;
                case "6":
                    Console.WriteLine("Июнь - 31 день");
                    break;
                case "7":
                    Console.WriteLine("Июль - 31 день");
                    break;
                case "8":
                    Console.WriteLine("Август - 31 день");
                    break;
                case "9":
                    Console.WriteLine("Сентябрь - 30 день");
                    break;
                case "10":
                    Console.WriteLine("Октябрь - 31 день");
                    break;
                case "11":
                    Console.WriteLine("Ноябрь - 30 день");
                    break;
                case "12":
                    Console.WriteLine("Декабрь - 31 день");
                    break;
                default:break;
            }
        }
    }
}
