﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Лаба_4
{
    class Program
    {
        static void Main(string[] args)
        {
            Task1(1, 2, 3, 4);
            Task1(1, 2, 3);
            int a = 99;
            Console.WriteLine(Task2(a));
            Console.ReadKey();
        }

        private static void Task1(params double[] el)
        {
            double average = 0;
            foreach (double it in el)
            {
                average += it;
            }
            Console.WriteLine(average / el.Length);
        }
        private static int Task2(int a)
        {
            if (a == 9)
                return 0;
            else return a + Task2(--a);
        }
    }
}
