﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ТренировкаRegex
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Введите строку, в которой надо что-то найти");
            string a = Console.ReadLine();
            string[] b = a.Split();
            Console.WriteLine("Введите ckj, которую надо найти");
            a = Console.ReadLine();
            Regex regex = new Regex(a, RegexOptions.IgnoreCase);
            
            int i = 0;
            foreach(var it in b)
            {
                if (regex.IsMatch(it))
                    i++;
            }
            Console.WriteLine("Количество вхождений равно "+i);
        }
    }
}
