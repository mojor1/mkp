﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР14
{
    class Program
    {
       
        
        static void Main(string[] args)
        {
            try
            {
                Func<List<int>, double> func;
                List<int> vs = new List<int>();
                Console.WriteLine("Введите количество чисел с которыми вы хотите произвести вычисления");
                int a = int.Parse(Console.ReadLine());
                Console.WriteLine("Введите сами числа");
                func = Agrigator.Arifm;
                for (int i = 0; i < a; i++)
                {
                    vs.Add(int.Parse(Console.ReadLine()));
                }
                Console.WriteLine("1.Среднее арифмитическое");
                Console.WriteLine("2.Среднее геометрическое");
                string b = Console.ReadLine();
                switch (b)
                {
                    case "1":
                        func = Agrigator.Arifm;
                        break;
                    case "2":
                        func = Agrigator.Geom;
                        break;
                    default: break;
                }
                Console.WriteLine("Ваш результат: " + func(vs));
            }
            catch
            {
                Console.WriteLine("Что-то пошло не так");
            }
        }
    }
}
