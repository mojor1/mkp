﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР14
{
    static class Agrigator
    {
      public  static double Arifm(List<int> vs)
        {
           double sum = 0;
            foreach (int a in vs)
                sum += a;
            return sum / vs.Count;
        }
       public static double Geom(List<int> vs)
        {
            double sum = 1;
            foreach (int a in vs)
                sum *= a;
            double b = 1 / (double)vs.Count;
            return Math.Round(Math.Pow(sum,b), 3);
        }
    }
}
