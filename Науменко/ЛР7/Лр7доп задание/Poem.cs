﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices.ComTypes;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace Лр7доп_задание
{
    class Poem
    {
        List<int> vs2 = new List<int>();
        public static string text = "У лукоморья дуб зелёный;\n" +
"Златая цепь на дубе том:\n" +
"И днём и ночью кот учёный\n" +
"Всё ходит по цепи кругом;\n" +
"Идёт направо — песнь заводит,\n" +
"Налево — сказку говорит.\n" +
"Там чудеса: там леший бродит,\n" +
"Русалка на ветвях сидит;\n" +
"Там на неведомых дорожках\n" +
"Следы невиданных зверей;\n" +
"Избушка там на курьих ножках\n" +
"Стоит без окон, без дверей;\n" +
"Там лес и дол видений полны;\n" +
"Там о заре прихлынут волны\n" +
"На брег песчаный и пустой,\n" +
"И тридцать витязей прекрасных\n" +
"Чредой из вод выходят ясных,\n" +
"И с ними дядька их морской;\n" +
"Там королевич мимоходом\n" +
"Пленяет грозного царя;\n" +
"Там в облаках перед народом\n" +
"Через леса, через моря\n" +
"Колдун несёт богатыря;\n" +
"В темнице там царевна тужит,\n" +
"А бурый волк ей верно служит;\n" +
"Там ступа с Бабою Ягой\n" +
"Идёт, бредёт сама собой,\n" +
"Там царь Кащей над златом чахнет;\n" +
"Там русский дух… там Русью пахнет!\n" +
"И там я был, и мёд я пил;\n" +
"У моря видел дуб зелёный;\n" +
"Под ним сидел, и кот учёный\n" +
"Свои мне сказки говорил.";
        public string Zammena()
        {
            string[] mastext = text.Split('\n');
            for (int j = 0; j < mastext.Length; j++)
            {
                string[] vs = mastext[j].Split();
                for (int i = 0; i < vs.Length; i++)
                {
                    if (mastext[i].Length > 3)
                        mastext[i].ToCharArray()[3] = '#';
                    if (mastext[i].Length > 6)
                        mastext[i].ToCharArray()[6] = '#';
                }
                mastext[j] = string.Join(" ", vs);
            }
            text = String.Join("\n", mastext);
            return text;
        }
        public void Kol()
        {
            bool flag = true;
            int kol = 0;
            int i = 0;
            string[] mastext = text.Split();
            string[] mastext2 = new string[mastext.Length];
            foreach (string it in mastext)
            {
                foreach(string ip in mastext2)
                {
                    if (ip == it)
                        flag = false;
                }
                if (flag)
                {
                    foreach (string ii in mastext)
                        if (it == ii)
                        {
                            kol++;

                        }
                    Console.WriteLine(it + '-' + kol);
                    kol = 0;
                    mastext2[i] = it;
                    i++;
                }
                flag = true;
                
            }
        }
        public void Proverka()
        {
            string[] mass = text.Split();
            foreach(var it in mass)
                if(this.Shablon(it, out string slovo))
                    Console.WriteLine("Слово начинающиеся на гласную и заканчивающиеся на согласную - "+ slovo);
        }
        private bool Shablon(string vs, out string vs1)
        {
          
            vs1 = "";
            if (vs.Last() == ',' || vs.Last() == '.' || vs.Last() == ';' || vs.Last() == ':' || vs.Last() == '?' || vs.Last() == '-' || vs.Last() == '!')
                for (int i = 0; i < vs.Length - 1; i++)
                    vs1 += vs[i];
            else vs1 = vs;
            if (vs1[0] == 'а' || vs1[0] == 'е' || vs1[0] == 'ё' || vs1[0] == 'у' || vs1[0] == 'я' || vs1[0] == 'ю' || vs1[0] == 'ы' || vs1[0] == 'э' || vs1[0] == 'о' || vs1[0] == 'и')
                if ((vs1[vs1.Length - 1] != 'а' || vs1[vs1.Length - 1] != 'е' || vs1[vs1.Length - 1] != 'ё' || vs1[vs1.Length - 1] != 'у' || vs1[vs1.Length - 1] != 'я' || vs1[vs1.Length - 1] != 'ю' || vs1[vs1.Length - 1] != 'ы' || vs1[0] == 'э' || vs1[vs1.Length - 1] != 'о' || vs1[vs1.Length - 1] != 'и') && vs1.Length!=1)
                    return true;
                else return false;
            else return false;
        }
        public string Slow(string text)
        {
            string f = "";
            int j;
            Random random = new Random();
            for(int i =0; i<100000; i++)
            {
                j = random.Next(0, text.Split().Length);
                f += text.Split()[j]+" ";
                vs2.Add(j);
            }
            return f;
        }
        public string Fast(string text)
        {
            StringBuilder f = new StringBuilder();
            foreach(int j in vs2)
            { 
                f.Append(new StringBuilder(text[j].ToString()+" "));
            }
            return f.ToString();
        }
    }

}
