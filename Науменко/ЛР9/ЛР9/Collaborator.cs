﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР9
{
    struct Collaborator
    {
        public string sername;
        public string name;
        public string patronymic;
        public Posts post;
        public int born_year;
        public double wages;
        public Collaborator(string name, string sername, string patronymic, string post, int born_year, double wages)
        {
            this.sername = sername;
            this.name = name;
            this.patronymic = patronymic;
            int i = 0;
            this.post = 0;
            foreach (string it in Enum.GetNames(typeof(Posts)))
            {
                if (it == post)
                {
                    this.post = (Posts)i;
                    i++;
                }
                i++;
            }
            this.born_year = born_year;
            this.wages = wages;

        }
    }
}
