﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;

namespace ЛР9
{
    class Program
    {
       
        
        static void Main(string[] args)
        {
            Collaborator[] collaborators = new Collaborator[1];
            bool flag4 = true;
            while (flag4)
            {
                Console.WriteLine("1) Ввод массива структур");
                Console.WriteLine("2) Изменение заданной структуры");
                Console.WriteLine("3) Удаление структуры из массива");
                Console.WriteLine("4) Вывод на экран массива структур");
                Console.WriteLine("5) Вывести количество однофамильцев по каждой фамилии");
                Console.WriteLine("6) Вывести общую заработную плату по каждой должности");
Console.WriteLine("7) Выход.");
                string c = Console.ReadLine();
                switch (c)
                {
                    case "1":
                        collaborators = Inicialisate();
                        break;
                    case "2":
                        Zamena(collaborators);
                        break;
                    case "3":
                        collaborators = Delete(collaborators);
                        break;
                    case "4":
                        Vivod(collaborators);
                        break;
                    case "5":
                        Kol(collaborators);
                        break;
                    case "6":
                        Sum(collaborators);
                        break;
                    case "7":
                        flag4 = false;
                        break;
                    default: break;
                }
            }
        }

        private static void Zamena(Collaborator[] collaborators)
        {
            Console.WriteLine("Введите имя, фамилию и отчество сотрудника");
            string name1 = Console.ReadLine();
            bool flag3 = true;
            if (name1.Split().Length == 3)
            {
                for (int p = 0; p < collaborators.Length; p++)
                    if (collaborators[p].name != name1.Split()[0] && collaborators[p].sername != name1.Split()[1] && collaborators[p].patronymic != name1.Split()[2])
                        flag3 = false;
                    else if (collaborators[p].name == name1.Split()[0] && collaborators[p].sername == name1.Split()[1] && collaborators[p].patronymic == name1.Split()[2])
                    {
                        Console.WriteLine("Введите новую должность и заработную плату сотрудника, через энтер соответственно");
                        string post = Console.ReadLine();
                        collaborators[p].post = 0;
                        int i = 0;
                        foreach (string it in Enum.GetNames(typeof(Posts)))
                        {
                            if (it == post)
                            {
                                collaborators[p].post = (Posts)i;
                                i++;
                            }
                            i++;
                        }
                        collaborators[p].wages = float.Parse(Console.ReadLine());
                    }

            }
            else flag3 = false;
            if (!flag3)
                Console.WriteLine("Такого сотрудника нет");
        }

        private static void Sum(Collaborator[] collaborators)
        {
            List<Posts> posts = new List<Posts>();
            bool flag1 = true;
            foreach (var it in collaborators)
            {
                foreach (var ip in posts)
                    if (it.post == ip)
                        flag1 = false;
                if (flag1)
                    posts.Add(it.post);
            }
            double plat = 0;
            foreach (var ip in posts)
            {
                foreach (var it in collaborators)
                    if (it.post == ip)
                        plat += it.wages;
                Console.WriteLine($"Общая заработная плата по должности {ip} равна {plat}");
                plat = 0;
            }
        }

        private static void Kol(Collaborator[] collaborators)
        {
            bool flag = true;
            int kol = 0;
            int k = 0;
            Collaborator[] collaborators2 = new Collaborator[collaborators.Length];
            foreach (var it in collaborators)
            {
                foreach (var ip in collaborators2)
                {
                    if (ip.sername == it.sername)
                        flag = false;
                }
                if (flag)
                {
                    foreach (var ip in collaborators)
                        if (it.sername == ip.sername)
                        {
                            kol++;

                        }
                    Console.WriteLine(it.sername + '-' + kol);
                    kol = 0;
                    collaborators2[k] = it;
                    k++;
                }
                flag = true;
            }
        }

        private static void Vivod(Collaborator[] collaborators)
        {
            foreach (var it in collaborators)
                Console.WriteLine($"{it.name} {it.sername} {it.patronymic} должность - {it.post} год рождения {it.born_year} зароботная плата - {it.wages}");
        }

        private static Collaborator[] Delete(Collaborator[] collaborators)
        {
            Console.WriteLine("Введите имя фамилию и отчество");
            bool flag2 = false;
            string name = Console.ReadLine();
            if (name.Split().Length == 3)
            {
                foreach (var it in collaborators)
                    if (it.name != name.Split()[0] && it.sername != name.Split()[1] && it.patronymic != name.Split()[2])
                        flag2 = true;
            }
            Collaborator[] collaborators1 = collaborators;
            if(collaborators1.Length>1)
            collaborators = new Collaborator[collaborators1.Length - 1];
            else collaborators = new Collaborator[1];
            int j = 0;
            if (flag2)
            {
                foreach (var it in collaborators1)
                {
                    if (it.name != name.Split()[0] && it.sername != name.Split()[1] && it.patronymic != name.Split()[2])
                    {
                        collaborators[j] = it;
                        j++;
                    }
                }
            }
            else Console.WriteLine("Такого сотрудника нет");
            return collaborators;
        }

        private static Collaborator[] Inicialisate()
        {
            Collaborator[] collaborators;
            Console.WriteLine("Введите количество работников");
            int b = int.Parse(Console.ReadLine());
            collaborators = new Collaborator[b];
            foreach (string it in Enum.GetNames(typeof(Posts)))
                Console.WriteLine(it);
                    for (int i = 0; i < b; i++)
            {
                        Console.WriteLine("Введите имя, фамилию, отчество, название должности год рождения и заработную плату соответственно через энтер");
                         collaborators[i] = new Collaborator(Console.ReadLine(), Console.ReadLine(), Console.ReadLine(), Console.ReadLine(), int.Parse(Console.ReadLine()), double.Parse(Console.ReadLine()));
            }

            return collaborators;
        }
    }
}