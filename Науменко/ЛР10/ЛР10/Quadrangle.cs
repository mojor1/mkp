﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР10
{
    class Quadrangle:Triangle
    {
       public int d;
        public double e;
        public double f;
        public Quadrangle(int a, int b, int c, int d, double e, double f) : base( a, b)
        {
            if(c>0)
            this.c = c;
            else throw new Exception("Нет такой фигуры!");
            if (d > 0)
                this.d = d;
            else throw new Exception("Нет такой фигуры!");
            if (e > 0)
                this.e = e;
            else throw new Exception("Нет такой фигуры!");
            if (f > 0)
                this.f = f;
            else throw new Exception("Нет такой фигуры!");
        }
        public override int Perimetr()
        {
            return a + b + c+d;
        }
        public override double Ploshad()
        {
            return Math.Sqrt((4 * Math.Pow(e, 2) * Math.Pow(f, 2) - Math.Pow((Math.Pow(b, 2) + Math.Pow(d, 2) - Math.Pow(a, 2) - Math.Pow(c, 2)), 2)) / 16);
        }
        public override string Information()
        {
             return $"стороны четырёхугольника - {a}, {b}, {c}, {d}, диаганали - {e}, {f} периметр - {Perimetr()}, площадь - {Ploshad()}";
        }
    }
}
