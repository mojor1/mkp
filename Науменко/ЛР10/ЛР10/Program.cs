﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР10
{
    class Program
    {
        static void Main(string[] args) 
        {
            Console.WriteLine("Введите сторроны треугольника");
            Triangle triangle = new Triangle(int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()));
            Console.WriteLine(triangle.Information());
            Console.WriteLine("Введите сторроны четырёхугольника и его диагонали");
            Quadrangle quadrangle = new Quadrangle(int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()));
            Console.WriteLine(quadrangle.Information());
        }
    }
}
