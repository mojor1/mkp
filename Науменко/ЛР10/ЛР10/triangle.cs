﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР10
{
    class Triangle
    {
       public int a;
       public int b;
       public int c;
        public Triangle(int a, int b, int c)
        {
            if (a > 0)
                this.a = a;
            else throw new Exception("Нет такой фигуры!");
            if(b>0)
            this.b = b;
            else throw new Exception("Нет такой фигуры!");
            if (b+a>c)
            this.c = c;
            else throw new Exception("Нет такой фигуры!");
        }
        public Triangle(int a, int b)
        {
            if (a > 0)
                this.a = a;
            else new Exception("Нет такой фигуры!");
            if (b > 0)
                this.b = b;
            else new Exception("Нет такой фигуры!");
        }
        public virtual int Perimetr()
        {
            return a + b + c;
        }
        public virtual double Ploshad()
        {
            double p= Perimetr()/2;
            return Math.Sqrt(p * (p - a) * (p - b) * (p - c));
        }
        public virtual string Information()
        {
            return $"стороны треугольника - {a}, {b}, {c}, периметр - {Perimetr()}, площадь - {Ploshad()}";
        }
    }
}
