﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР10._2
{
    class Experienced : Person
    {
        public Experienced(string name, int age, int stage) : base(name, age, stage)
        {

        }
        public override bool Fire()
        {
            bool flag = false;
            double ver = 0.05 * stage;
            Random random = new Random();
            if (random.NextDouble() <= ver)
                flag = true;
            return flag;
        }
        public override string Information(out bool flag)
        {
            flag = Fire();
            return $"Опытный {name}, возвраст {age},стаж {stage},попадание: {flag}";
        }
    }
}
