﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР10._2
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Person> people = new List<Person>();

            int i = 1;
            while (i <= 7)
            {
                if (i % 7 == 1 || i % 7 == 5 || i % 7 == 6)
                {
                    Console.WriteLine("Введите имя новичка его возвраст и стаж стрельбы через энтер соотьветственно (стаж стрельбы и возвраст выражается целыми числами в годах)");
                    Novice novice = new Novice(Console.ReadLine(), int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()));
                    people.Add(novice);
                }
                if(i % 7 == 2 || i % 7 == 4 || i % 7 == 0)
                {
                    Console.WriteLine("Введите имя опытного его возвраст и стаж стрельбы через энтер соотьветственно (стаж стрельбы и возвраст выражается целыми числами в годах)");
                    Experienced novice = new Experienced(Console.ReadLine(), int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()));
                    people.Add(novice);
                }
                if (i % 7 == 3)
                {
                    Console.WriteLine("Введите имя ветерана его возвраст и стаж стрельбы через энтер соотьветственно (стаж стрельбы и возвраст выражается целыми числами в годах)");
                    Veteran novice = new Veteran(Console.ReadLine(), int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()));
                    people.Add(novice);
                }
                i++;
            }
            bool flag = false;
            while (!flag)
            {
                foreach (Person it in people)
                {
                    Console.WriteLine(it.Information(out flag));
                    if (flag)
                        break;
                }
            }
            Console.ReadKey();
        }
    }
}
