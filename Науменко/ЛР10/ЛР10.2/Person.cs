﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР10._2
{
    abstract class Person
    {
        public string name;
        public int age;
        public int stage;
        public Person(string name, int age, int stage)
        {
            this.name = name;
            this.age = age;
            this.stage = stage;
        }
        public abstract bool Fire();
        public abstract string Information(out bool flag);
    }
}
