﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Matrix;

namespace Срез
{
    class Program
    {
        static void Main(string[] args)
        {
            Matrix.Matrix matrix = new Matrix.Matrix(2, 3);
            matrix.Print();
            Console.WriteLine(matrix.SumNegative());
            Console.WriteLine(matrix.MinInRow(1));
        }
    }
}
