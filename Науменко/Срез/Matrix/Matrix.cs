﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Matrix
{
  public  class Matrix
    {
        int index1;
        int index2;
        int[,] mas;
        public Matrix(int index1, int index2)
        {
            this.index1 = index1;
            this.index2 = index2;
            mas = new int[index1, index2];
            Random random = new Random();
            for (int i = 0; i < index1; i++)
                for (int j = 0; j < index2; j++)
                    mas[i, j] = random.Next(-10, 10);
        }
        public double SumNegative()
        {
            double sum = 0;
            for (int i = 0; i < index1; i++)
                for (int j = 0; j < index2; j++)
                    if (mas[i, j] < 0)
                        sum += mas[i, j];
            return sum;
        }
        public int MinInRow(int row)
        {
            int min = mas[row-1, 0];
            for (int j = 0; j < index2; j++)
                if (min > mas[row-1, j])
                    min = mas[row-1, j];
            return min;
        }
        public void Print()
        {
            for (int i = 0; i < index1; i++)
            {
                for (int j = 0; j < index2; j++)
                {
                    Console.Write(mas[i, j] + " ");
                }
                Console.WriteLine();
            }
        }
    }
}
