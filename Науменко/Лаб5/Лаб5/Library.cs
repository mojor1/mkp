﻿using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Лаб5
{
    class Library
    {
        List<Polka> books = new List<Polka>();
        public void Add(Polka book)
        {
            books.Add(book);
        }
        public void Add(Book book)
        {
            bool flag = false;
            foreach (Polka polka in books)
                if (polka.Ganer == book.genre)
                {
                    flag = polka.Add(book);
                }
            if (!flag)
            {
                Polka polka = new Polka(book);
                books.Add(polka);
            }

        }
        public void Add(string Author, DateTime god, string Name, string Gener)
        {
            bool flag = false;
            foreach (Polka polka in books)
                if (polka.Ganer == Gener)
                {
                    flag = polka.Add(Author, god, Name, Gener);
                }
            if (!flag)
            {
                Polka polka = new Polka(Author, god, Name, Gener);
                books.Add(polka);
            }
        }
            public Library(List<Polka> books)
        {
                this.books = books;
        }
        public void Add(List<Polka> books)
        {
             foreach (Polka book in books)
                    this.books.Add(book);
        }
        public Library() { }
        public void SortbyName()
        {
            foreach (Polka polka in books)
                polka.SortbyName();
        }
        public void SortbyAuthor()
        {
            foreach (Polka polka in books)
                polka.SortbyAuthor();
        }
        public void SortbyGenre()
        {
            var orderedDatas = books.OrderBy(x => x.Ganer).ToList();
            foreach (Polka polka in orderedDatas)
                polka.Vivod();
            
        }
        public void SortbyGod()
        {
            foreach (Polka polka in books)
                polka.SortbyGod();
        }
        public bool Poisk(string name)
        {
            bool flag = false;
            foreach (Polka book in books)
                if (book.Poisk(name))
                {
                    flag = true;
                }
            if (!flag)
                return false;
            else return true;
        }
        public bool Poisk(DateTime god)
        {
            bool flag = false;
            foreach (Polka book in books) { 
                if (book.Poisk(god))
                    flag = true;
                
            }
            if (!flag)
                return false;
            else return true;
        }
        public bool PoiskAuthor(string author)
        {
            bool flag = false;
            foreach (Polka book in books)
                if (book.PoiskAuthor(author)) { 
                    flag = true;
                   
                }
            if (!flag)
                return false;
            else return true;
        }
        public bool PoiskGenre(string gener)
        {
            bool flag = false;
            foreach (Polka book in books)
                if (book.Ganer==gener) { 
                    flag = true;
                    book.Vivod();
                }
            if (!flag)
                return false;
            else return true;
        }
        public bool Delete(string name, string author)
        {
            bool flag = false;
            foreach (Polka it in books)
            {
                it.Delete(name, author);
                flag = true;
            }
            if (!flag)
                return false;
            else return true;
        }
        public void Vivod()
        {
            foreach (Polka polka in books)
                polka.Vivod();
        }
    }
}
