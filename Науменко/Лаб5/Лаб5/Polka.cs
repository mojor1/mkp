﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Лаб5
{
    class Polka
    {
        List<Book> polka = new List<Book>();
        public string Ganer { get; set; }
        public int Kol { get; set; }
        public bool Add(Book book)
        {
            if (Kol <= 40 && book.genre == Ganer)
            {
                polka.Add(book);
                Kol++;
                return true;
            }
            else return false;
        }
        public bool Add(string Author, DateTime god, string Name, string Gener)
        {
            if (Kol <= 40 && Gener == Ganer)
            {
                Book book = new Book(Author, god, Name, Gener);
                polka.Add(book);
                Kol++;
                return true;
            }
            else return false;
        }
        public Polka(Book book)
        {
            Ganer = book.Genre;
            polka.Add(book);
            Kol++;
        }
        public Polka(string ganer)
        {
            Ganer = ganer;
        }
        public Polka(string Author, DateTime god, string Name, string Gener)
        {
            Book book = new Book(Author, god, Name, Gener);
            Ganer = book.Genre;
            polka.Add(book);
            Kol++;
        }
        public void SortbyName()
        {
            var orderedDatas = polka.OrderBy(x => x.Name).ToList();
            polka = orderedDatas;
            this.Vivod();
        }
        public void Vivod()
        {
            Console.WriteLine(Ganer + ":");
            foreach (Book book in polka)
                book.ShowBook();
        }
        public void SortbyAuthor()
        {
            var orderedDatas = polka.OrderBy(x => x.Author).ToList();
            polka = orderedDatas;
            this.Vivod();
        }
        public void SortbyGod()
        {
            var orderedDatas = polka.OrderBy(x => x.god).ToList();
            polka = orderedDatas;
            this.Vivod();
        }
        public bool Poisk(string name)
        {
            bool flag = false;
            foreach (Book book in polka)
                if (book.Poisk(name))
                {
                    flag = true;
                }
            if (!flag)
                return false;
            else return true;
        }
        public bool Poisk(DateTime god)
        {
            bool flag = false;
            foreach (Book book in polka)
            {
                if (book.Poisk(god))
                    flag = true;

            }
            if (!flag)
                return false;
            else return true;
        }
        public bool PoiskAuthor(string author)
        {
            bool flag = false;
            foreach (Book book in polka)
                if (book.PoiskAuthor(author))
                {
                    flag = true;

                }
            if (!flag)
                return false;
            else return true;
        }
        public bool Delete(string name, string author)
        {
            bool flag = false;
            foreach (Book it in polka)
            {
                if (it.Name == name && it.Author == author)
                {
                    polka.Remove(it);
                    flag = true;
                    break;
                }
            }
            if (!flag)
                return false;
            else return true;
        }
    }
}
