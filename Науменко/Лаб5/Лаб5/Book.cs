﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Лаб5
{
    class Book
    {
        public string Author { get; set; }
        public DateTime god = new DateTime();
        public DateTime God {
            get { return god; }
            set { DateTime god1= new DateTime(1570, 1, 1);
                bool flag = false;
                while (!flag) {
                    if (value > god1 && value<=DateTime.Now)
                    {
                        god = value;
                        flag=true;
                    }
                    else
                    {
                        Console.WriteLine("Введён некоректный год. Введите ещё раз");
                        value = DateTime.Parse(Console.ReadLine());
                    }
                }
            }
        }
        public string Name { get; set; }
        public string genre;
        public string Genre
        {
            get
            {
                return genre;
            }
            set
            {
                while (true)
                {
                    if (value == "фантастика" || value == "роман" || value == "фэнтази" || value == "автобиография" || value == "антиутопия" || value == "бизнес и деньги" || value == "драмма" || value == "детские книги" || value == "другое" || value == "история")
                    {
                        genre = value;
                        break;
                    }
                    else
                    {
                        Console.WriteLine("Введён некоректный жанр. Введите ещё раз");
                        value = Console.ReadLine();
                    }
                }
            }
        }
        public Book(string author, DateTime god, string name, string Gener)
        {
            Author = author;
            God = god;
            Name = name;
            this.Genre = Gener;
        }
        public bool Poisk(string name)
        {
            if (name == this.Name)
            {
                this.ShowBook();
                return true;
            }
            else return false;
        }
        public bool Poisk(DateTime data)
        {
            if (data == this.god)
            {
                this.ShowBook();
                return true;
            }
            else return false;
        }
        public bool PoiskGenre(string ganer)
        {
            if (ganer == this.genre)
            {
                this.ShowBook();
                return true;
            }
            else return false;
        }
        public bool PoiskAuthor(string author)
        {
            if (author == this.Author)
            {
                this.ShowBook();
                return true;
            }
            else return false;
        }
        public void ShowBook()
        {
            Console.WriteLine($"Книга: {this.Name} Автор: {this.Author} Жанр:{this.genre} Год издания:{this.God.ToString("d")}");
        }
       
    }
}
