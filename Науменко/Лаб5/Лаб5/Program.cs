﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Лаб5
{
    class Program
    {
        static void Main(string[] args)
        {
            bool flag = false;
            Library library = new Library();
            while (!flag)
            {
                Console.WriteLine("1 Добавить запись о книге");
                Console.WriteLine("2 Удалить запись о книге");
                Console.WriteLine("3 Поиск книги");
                Console.WriteLine("4 Сортировка книг и вывод их на экран");
                Console.WriteLine("5 вывод информации о книгах на экран");
                Console.WriteLine("6 Выход");
                string a =Console.ReadLine();
                switch (a)
                {
                    case "1":
                        Console.WriteLine("Введите название книги");
                        string name=Console.ReadLine();
                        Console.WriteLine("Введите автора книги");
                        string author = Console.ReadLine();
                        Console.WriteLine("Введите жанр книги");
                        string genre = Console.ReadLine();
                        Console.WriteLine("Введите дату издания книги");
                        DateTime date = DateTime.Parse(Console.ReadLine());
                        library.Add(author, date, name, genre);
                        break;
                    case "2":
                        Console.WriteLine("Введите название книги");
                        string name1 = Console.ReadLine();
                        Console.WriteLine("Введите автора книги");
                        string author1 = Console.ReadLine();
                        if(!library.Delete(name1, author1))
                        Console.WriteLine("Такой книги нет в списке!");
                        break;
                    case "3":
                        Console.WriteLine("1 Поиск по автору");
                        Console.WriteLine("2 Поиск по названию");
                        Console.WriteLine("3 Поиск по дате");
                        Console.WriteLine("4 Поиск по жанру");
                        string b = Console.ReadLine();
                        switch (b)
                        {
                            case "1":
                                Console.WriteLine("Введите автора книги");
                                string author2 = Console.ReadLine();
                               if(!library.PoiskAuthor(author2))
                                    Console.WriteLine("Такой книги нет в списке!");
                                break;
                            case "2":
                                Console.WriteLine("Введите название книги");
                                string name2 = Console.ReadLine();
                                if(!library.Poisk(name2))
                                Console.WriteLine("Такой книги нет в списке!");
                                break;
                            case "3":
                                Console.WriteLine("Введите дату издания книги");
                                DateTime date1 = DateTime.Parse(Console.ReadLine());
                               if(!library.Poisk(date1))
                                    Console.WriteLine("Такой книги нет в списке!");
                                break;
                            case "4":
                                Console.WriteLine("Введите жанр книги");
                                string genre1 = Console.ReadLine();
                               if(!library.PoiskGenre(genre1))
                                    Console.WriteLine("Такой книги нет в списке!");
                                break;
                            default: Console.WriteLine( "Такого пункта нет"); break;
                        }
                        break;
                    case "4":
                        Console.WriteLine("1 Сортировка по названию");
                        Console.WriteLine("2 Сортировка по автору");
                        Console.WriteLine("3 Сортировка по году издания");
                        Console.WriteLine("4 Сортировка по жанру");
                        string c = Console.ReadLine();
                        switch (c)
                        {
                            case "1":
                                library.SortbyName();
                                break;
                            case "2":
                                library.SortbyAuthor();
                                break;
                            case "3":
                                library.SortbyGod();
                                break;
                            case "4":
                                library.SortbyGenre();
                                break;
                            default: Console.WriteLine("Такого пункта нет"); break;
                        }
                        break;
                    case "6":
                        flag = true;
                        break;
                    case "5":
                        library.Vivod();
                        break;
                    default: Console.WriteLine("Такого пункта нет"); break;
                }
            }
        }
    }
}
