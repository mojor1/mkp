﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР3
{
    class Cezar
    {
        string[] alphabet = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "O", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "x", "Y", "Z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10", " "};
        List<string> codalphabet;
        string key;
        int smeshenie;
        public Cezar(string key, int smeshenie)
        {
            this.key = key;
            this.smeshenie = smeshenie;
            CodeAlphabet();
        }
        void CodeAlphabet()
        {
            codalphabet = new List<string>();
            
            for(int i= alphabet.Length-1- smeshenie; i<alphabet.Length; i++)
            {
                codalphabet.Add(alphabet[i]);
            }
            for (int i = 0; i < alphabet.Length - smeshenie; i++)
                codalphabet.Add(alphabet[i]);
            for (int j = key.Length-1; j >=0; j--) {
                string k = "";
                k += key[j];
                foreach (var it in codalphabet)
                    if (k == it)
                    {
                        codalphabet.Remove(it);
                        break;
                    }
                codalphabet.Insert(0, k);
            }
               
        }
        public string Cod(string wards, string Filename)
        {
            string newwards = "";
            for(int i=0; i<wards.Length; i++)
            {
                string k = "";
                k += wards[i];
                for (int j = 0; j < alphabet.Length; j++)
                    if (k == alphabet[j])
                        newwards += codalphabet.ToArray()[j];

            }
            File.WriteFile(Filename, newwards);
            return newwards; 
        }
        public string DeCod(string wards)
        {
            string newwards = "";
            for (int i = 0; i < wards.Length; i++)
            {
                string k = "";
                k += wards[i];
                for (int j = 0; j < codalphabet.ToArray().Length; j++)
                    if (k == codalphabet.ToArray()[j])
                        newwards += alphabet[j];

            }
            return newwards;
        }
        public string DeCodFile(string Filename)
        {
            string wards = File.ReadFile(Filename);
            string newwards = "";
            for (int i = 0; i < wards.Length; i++)
            {
                string k = "";
                k += wards[i];
                for (int j = 0; j < codalphabet.ToArray().Length; j++)
                    if (k == codalphabet.ToArray()[j])
                        newwards += alphabet[j];

            }
            return newwards;
        }
    }
}
