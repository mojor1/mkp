﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР3
{
    class Program
    {
        static void Main(string[] args)
        {
            bool flag = true;
            while (flag)
            {
                Console.WriteLine("1.Зашифровать");
                Console.WriteLine("2.Расшифровать");
                Console.WriteLine("3.Выйти");
                string a = Console.ReadLine();
                switch (a)
                {
                    case "1":
                        try
                        {
                            Console.WriteLine("Введите ключ для шифрования (CAPS)");
                            string key = Console.ReadLine().ToUpper();
                            Console.WriteLine("Введите смещение для шифрования");
                            int smeshenie = int.Parse(Console.ReadLine());
                            Console.WriteLine("Введите фразу которую хотите зашифровать (из больших латинских букв и цифр)");
                            string wards = Console.ReadLine().ToUpper();
                            Console.WriteLine("ВВедите путь к файлу и название файла в который хотите записать зашифрованную фравзу");
                            string filename = Console.ReadLine();
                            Cezar cezar = new Cezar(key, smeshenie);
                            Console.WriteLine(cezar.Cod(wards, filename));
                        }
                        catch
                        {
                            Console.WriteLine("Вы что-то сделали не так!");
                        }
                        break;
                    case "2":
                        try
                        {
                            Console.WriteLine("Введите ключ для расшифрования");
                            string key = Console.ReadLine().ToUpper();
                            Console.WriteLine("Введите смещение для расшифрования");
                            int smeshenie = int.Parse(Console.ReadLine());
                            bool flag1 = true;
                            while (flag1)
                            {
                                Console.WriteLine("1. Расшифровать вводимую фразу");
                                Console.WriteLine("1. Расшифровать фразу из файла");
                                string b = Console.ReadLine();
                                if (b == "1")
                                {
                                    Console.WriteLine("Введите фразу которую хотите расшифровать (из больших латинских букв и цифр)");
                                    string wards = Console.ReadLine().ToUpper();
                                    Cezar cezar = new Cezar(key, smeshenie);
                                    Console.WriteLine(cezar.DeCod(wards));
                                    flag1 = false;
                                }
                                else if (b == "2")
                                {
                                    Console.WriteLine("ВВедите путь к файлу и название файла в котором лежит зашифрованная фраза");
                                    string filename = Console.ReadLine();
                                    Cezar cezar = new Cezar(key, smeshenie);
                                    Console.WriteLine(cezar.DeCodFile(filename));
                                    flag1 = false;
                                }

                            }
                        }
                        catch
                        {
                            Console.WriteLine("Вы что-то сделали не так!");
                        }
                        break;
                    case "3":
                        flag = false;
                        break;
                    default: break;
                }
            }
        }
    }
}
