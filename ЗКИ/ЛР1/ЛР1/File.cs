﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Text;
using System.Threading;
using System.Configuration;

namespace ЛР1
{
   public  class File
    {
        string Filename { get; set; }
        List<User> users = new List<User>();
        //public File(string filename)
        //{
        //    Filename = filename;
        //}
        public static void LondingData(List<User> users)
        {
            XmlDocument newXDoc = new XmlDocument();
            XmlElement newXRoot = newXDoc.CreateElement("users");
            foreach (var user in users)
            {
                XmlElement xmlUserElement = newXDoc.CreateElement("user");
                XmlElement LoginElem = newXDoc.CreateElement("login");
                XmlElement CodPasswordElem = newXDoc.CreateElement("codpassword");
                XmlElement CodPincodElem = newXDoc.CreateElement("pincod");
                XmlText xmlAgeValue = newXDoc.CreateTextNode(user.Login);
                string password="";
                for (int i = 0; i < user.CodPassword.Length; i++)
                    if (i != user.CodPassword.Length - 1)
                        password += user.CodPassword[i].ToString() + " ";
                    else password += user.CodPassword[i].ToString();
                XmlText xmlCompanyValue = newXDoc.CreateTextNode(password);
                XmlText xmlPincodValue = newXDoc.CreateTextNode(user.Pincod.ToString());
                LoginElem.AppendChild(xmlAgeValue);
                CodPasswordElem.AppendChild(xmlCompanyValue);
                CodPincodElem.AppendChild(xmlPincodValue);
                xmlUserElement.SetAttribute("name", user.Name);
                xmlUserElement.AppendChild(LoginElem);
                xmlUserElement.AppendChild(CodPasswordElem);
                xmlUserElement.AppendChild(CodPincodElem);
                newXRoot.AppendChild(xmlUserElement);
            }

            newXDoc.AppendChild(newXRoot);
            newXDoc.Save("D://users1.xml");
        }
        public static List<User> GetData()
        {
            List<User> users = new List<User>();
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load("D://users1.xml");
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlElement xnode in xRoot)
            {
                User user = new User();
                XmlNode attr = xnode.Attributes.GetNamedItem("name");
                if (attr != null)
                    user.Name = attr.Value;
                foreach (XmlNode childnode in xnode.ChildNodes)
                {
                    if (childnode.Name == "login")
                        user.Login = childnode.InnerText;
                    if (childnode.Name == "codpassword")
                    {
                        string[] password= childnode.InnerText.Split();
                        List<int> vs = new List<int>();
                        foreach (var it in password)
                            vs.Add(int.Parse(it));
                        user.CodPassword = vs.ToArray();
                        user.Decod(user.CodPassword);
                    }
                    if (childnode.Name == "pincod")
                        user.Pincod = int.Parse(childnode.InnerText);

                }
                users.Add(user);
            }

            return users;
        }
    }
}
