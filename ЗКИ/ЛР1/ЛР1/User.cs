﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР1
{
    [Serializable]
   public class User
    {
        public string Name { get; set; }
        public string Login { get; set; }
        public string Password { get; set; }
        public int[] CodPassword { get; set; }
        public int Pincod { get; set; }
        public User(string name, string login, string password, int pincod)
        {
            Name = name;
            Login = login;
            Password = password;
            Pincod = pincod;
            this.Code(password);
        }
        public User(string name, string login, int[] password, int pincod)
        {
            Name = name;
            Login = login;
            this.Decod(password);
            Pincod = pincod;
            CodPassword = password;

        }
        public User(string name, string login)
        {
            Name = name;
            Login = login;
            this.Generate();
        }
        public User() { }
        public bool Testing(string login, string password)
        {
            if (Login == login && password == Password)
                return true;
            else return false;
        }
        public bool ForgetPassword(string login, int pincod)
        {
            if (Login == login && pincod == Pincod)
                return true;
            else return false;
        }
        public string Decod(int[] password)
        {
            foreach(int it in password)
            Password+= ((char)it).ToString();
            return Password;
        }
         public int[] Code(string password)
        {
            List<int> vs = new List<int>();
            foreach (char it in password)
            {
                vs.Add((int)it);
            }
            CodPassword = vs.ToArray();
            return CodPassword;
        }
        public void Generate()
        {
            Random random = new Random();
            int kol = random.Next(8, 15);
            List<int> vs = new List<int>();
            for(int i=0; i<kol; i++)
            {
                vs.Add(random.Next(48, 120));
            }
            CodPassword = vs.ToArray();
            this.Decod(CodPassword);
            Pincod = random.Next(1000, 9999);
        }
    }
}
