﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР1
{
    class Program
    {
        static void Main(string[] args)
        {
            List<User> users = new List<User>();
            bool flag = false;
            try
            {  
                users = File.GetData();
            }
            catch (System.IO.IOException)
            {
                Console.WriteLine("users1.Xml используется другой программой, пожалуйста закройте его!");
                 flag = true;
            }
            catch (System.Xml.XmlException)
            {
                Console.WriteLine("users1.Xml повреждён!");
                flag = true;
            }
            while (!flag)
            {
                Console.WriteLine("1 Зарегистрироваться");
                Console.WriteLine("2 Войти");
                string a = Console.ReadLine();
                switch (a)
                {
                    case "1":
                        Console.Clear();
                        Console.WriteLine("Введите имя");
                        string name = Console.ReadLine();
                        if ((int)name[0] == 32 || name[0] == ',' || name[0] == '.' || name[0] == '?' || name[0] == '!')
                        {
                            Console.WriteLine("Неправельныое имя");
                            break;
                        }
                        Console.WriteLine("Введите логин");
                        string login = Console.ReadLine();
                        if (login[0] == ' ' && login[0] == ',' && login[0] == '.' && login[0] == '?' && login[0] == '!')
                        {
                            Console.WriteLine("Неправельный логин");
                            break;
                        }
                        User user = new User(name, login);

                        Registration(users, user);
                        File.LondingData(users);

                        break;
                    case "2":
                        Console.Clear();
                        Console.WriteLine("1 Войти с паролем");
                        Console.WriteLine("2 Забыл пароль");
                        string b = Console.ReadLine();
                        switch (b)
                        {
                            case "1":
                        Console.WriteLine("Введите логин");
                        string login1 = Console.ReadLine();
                                Console.WriteLine("Введите пароль");
                                string password= Console.ReadLine();
                                foreach (User it in users)
                                    if (it.Testing(login1, password))
                                    { 
                                        flag = true;
                                        break;
                                    }
                                if(!flag)
                                    Console.WriteLine("Неправельный логин или пароль");
                                break;
                            case "2":
                                Console.WriteLine("Введите логин");
                                string login2 = Console.ReadLine();
                                Console.WriteLine("Введите пинкод");
                                int pincod = int.Parse(Console.ReadLine());
                                foreach (User it in users)
                                    if (it.ForgetPassword(login2, pincod))
                                    {
                                        flag = true;
                                        Console.WriteLine($"Ваш пароль {it.Password}. Больше не забывайте");
                                        Console.ReadKey();
                                        break;
                                    }
                                if (!flag)
                                    Console.WriteLine("Неправельный логин или пинкод");
                                break;
                            default: break;
                        }       
                break;
                    default: break;
                }
            }
        }

        private static void Registration(List<User> users, User user)
        {
            bool flag6 = true;
            foreach (User it in users)
                if (it.Login == user.Login)
                    flag6 = false;
            if (flag6)
            {
                users.Add(user);
                Console.WriteLine($"Ваш пароль {user.Password}, Ваш пинкод {user.Pincod}");
            }
            else Console.WriteLine("Такой пользователь существует");
        }
    }
}
