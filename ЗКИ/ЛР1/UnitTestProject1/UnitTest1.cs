﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Xml;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ЛР1;

namespace UnitTestProject1
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void FileReading()
        {
            List<User> users = new List<User>();
            User user1 = new User("Mихаил", "Mojor", "060602MG", 1649);
            users.Add(user1);
            XmlDocument newXDoc = new XmlDocument();
            XmlElement newXRoot = newXDoc.CreateElement("users");
            foreach (var user in users)
            {
                XmlElement xmlUserElement = newXDoc.CreateElement("user");
                XmlElement LoginElem = newXDoc.CreateElement("login");
                XmlElement CodPasswordElem = newXDoc.CreateElement("codpassword");
                XmlElement CodPincodElem = newXDoc.CreateElement("pincod");
                XmlText xmlAgeValue = newXDoc.CreateTextNode(user.Login);
                string password = "";
                for (int i = 0; i < user.CodPassword.Length; i++)
                    if (i != user.CodPassword.Length - 1)
                        password += user.CodPassword[i].ToString() + " ";
                    else password += user.CodPassword[i].ToString();
                XmlText xmlCompanyValue = newXDoc.CreateTextNode(password);
                XmlText xmlPincodValue = newXDoc.CreateTextNode(user.Pincod.ToString());
                LoginElem.AppendChild(xmlAgeValue);
                CodPasswordElem.AppendChild(xmlCompanyValue);
                CodPincodElem.AppendChild(xmlPincodValue);
                xmlUserElement.SetAttribute("name", user.Name);
                xmlUserElement.AppendChild(LoginElem);
                xmlUserElement.AppendChild(CodPasswordElem);
                xmlUserElement.AppendChild(CodPincodElem);
                newXRoot.AppendChild(xmlUserElement);
            }

            newXDoc.AppendChild(newXRoot);
            newXDoc.Save("D://users1.xml");
            List<User> users1 = File.GetData();
            bool flag = true;
            for (int i = 0; i < users.Count; i++)
                if (users.ToArray()[i].Login != users1.ToArray()[i].Login || users.ToArray()[i].Name != users1.ToArray()[i].Name || users.ToArray()[i].Password != users1.ToArray()[i].Password || users.ToArray()[i].Pincod != users1.ToArray()[i].Pincod)
                    flag = false;
            if (flag)
                Assert.AreEqual(1, 1);
            else Assert.Fail();

        }
        [TestMethod]
        public void FileWriting()
        {
            List<User> users = new List<User>();
            User user1 = new User("Mихаил", "Mojor", "060602MG", 1649);
            users.Add(user1);
            File.LondingData(users);
            List<User> users1 = File.GetData();
            bool flag = true;
            for (int i = 0; i < users.Count; i++)
                if (users.ToArray()[i].Login != users1.ToArray()[i].Login || users.ToArray()[i].Name != users1.ToArray()[i].Name || users.ToArray()[i].Password != users1.ToArray()[i].Password || users.ToArray()[i].Pincod != users1.ToArray()[i].Pincod)
                    flag = false;
            if (flag)
                Assert.AreEqual(1, 1);
            else Assert.Fail();
        }
        [TestMethod]
        public void UserTestingTrue()
        {
            User user1 = new User("Mихаил", "Mojor", "060602MG", 1649);
            bool expected = true;
            bool actual = user1.Testing("Mojor", "060602MG");
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void UserTestingFalse()
        {
            User user1 = new User("Mихаил", "Mojor", "060602MG", 1649);
            bool expected = false;
            bool actual = user1.Testing("Mojor1", "060602MG");
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void UserForgetPasswordTrue()
        {
            User user1 = new User("Mихаил", "Mojor", "060602MG", 1649);
            bool expected = true;
            bool actual = user1.ForgetPassword("Mojor", 1649);
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void UserForgetPasswordFalse()
        {
            User user1 = new User("Mихаил", "Mojor", "060602MG", 1649);
            bool expected = false;
            bool actual = user1.ForgetPassword("Mojor1", 1649);
            Assert.AreEqual(expected, actual);
        }
        [TestMethod]
        public void UserCode()
        {
            User user1 = new User();
            int[] actual = user1.Code("Mojor");
            List<int> vs = new List<int>();
            foreach (char it in "Mojor")
            {
                vs.Add((int)it);
            }
            int[] expected= vs.ToArray();
            bool flag = true;
            for (int i = 0; i < expected.Length; i++)
                if (expected[i] != actual[i])
                    flag = false;
            if(flag)
                Assert.AreEqual(1, 1);
            else Assert.Fail();
        }
        [TestMethod]
        public void UserDeCode()
        {
            User user1 = new User();
            int[] mas = { 10, 52, 76, 86, 54 };
            string actual = user1.Decod(mas);
            string expected = "";
             foreach (int it in mas)
                expected+= ((char)it).ToString();
            
                Assert.AreEqual(actual, expected);
        }
        [TestMethod]
        public void Registratoin()
        {
            List<User> users = new List<User>();
            User user1 = new User("aaa", "ппп");
            User user2 = new User("aaa", "ппп");
            users.Add(user1);users.Add(user2);
            if (users.Count > 1)
                Assert.Fail();
            else
            Assert.AreEqual(1, 1);
        }
    }
}
