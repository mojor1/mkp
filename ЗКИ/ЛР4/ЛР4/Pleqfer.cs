﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР4
{
    class Pleqfer
    {
         public string[] alphabet = { "A", "B", "C", "D", "E", "F", "G", "H", "I", "K", "L", "O", "M", "N", "P", "Q", "R", "S", "T", "U", "V", "W", "x", "Y", "Z", "0", "1", "2", "3", "4", "5", "6", "7", "8", "9", "10"};
         public string[] codalphabet = { "AA", "AB", "AC", "AD", "AE", "AF", "BA", "BB","BC", "BD", "BE", "BF", "CA", "CB","CC", "CD","CE", "CF", "DA","DB", "DC", "DD", "DE","DF", "EA","EB", "EC", "ED","EE","EF","FA","FB","FC","FD","FE","FF" };
        List<string> alphabetWhithKey = new List<string>();
        string key;    
        public Pleqfer(string key)
        {
            this.key = key;
            CodeAlphabet();
        }
        void CodeAlphabet()
        {
            alphabetWhithKey = alphabet.ToList();
            for (int j = key.Length - 1; j >= 0; j--)
            {
                string k = "";
                k += key[j];
                foreach (var it in codalphabet)
                    if (k == it)
                    {
                        alphabetWhithKey.Remove(it);
                        break;
                    }
                alphabetWhithKey.Insert(0, k);
            }

        }
        public string Cod(string wards, string Filename)
        {
            string newwards = "";
            for (int i = 0; i < wards.Length; i++)
            {
                string k = "";
                k += wards[i];
                for (int j = 0; j < alphabetWhithKey.Count; j++)
                    if (k == alphabetWhithKey.ToArray()[j])
                        newwards += codalphabet[j];

            }
            File.WriteFile(Filename, newwards);
            return newwards;
        }
        public string DeCod(string wards)
        {
            string newwards = "";
            for (int i = 0; i < wards.Length; i+=2)
            {
                string k = wards[i].ToString() + wards[i + 1].ToString();
                for (int j = 0; j < codalphabet.ToArray().Length; j++)
                    if (k == codalphabet.ToArray()[j])
                        newwards += alphabetWhithKey.ToArray()[j];

            }
            return newwards;
        }
        public string DeCodFile(string Filename)
        {
            string wards = File.ReadFile(Filename);
            string newwards = "";
            for (int i = 0; i < wards.Length; i+=2)
            {
                string k = wards[i].ToString() + wards[i + 1].ToString();
                for (int j = 0; j < codalphabet.ToArray().Length; j++)
                    if (k == codalphabet.ToArray()[j])
                        newwards += alphabetWhithKey.ToArray()[j];

            }
            return newwards;
        }
    }
}

