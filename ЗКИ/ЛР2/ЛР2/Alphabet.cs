﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace ЛР2
{
    class Alphabet
    {
        public string[] alphabet;
        public Alphabet(string name)
        {
            string file = File.ReadAllText(name);
            alphabet = file.Split(';');
        }
        public string[] Return()
        {
            return alphabet;
        }
    }
}
