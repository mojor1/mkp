﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace ЛР2
{
    class Polybius_square
    {
        public string[] alphabet;
        public string[] codealphabet;
        public Polybius_square(string[] alphabet)
        {
            this.alphabet = alphabet;
            Code();
        }
        public Polybius_square() { }
        private void Code()
        {
           double a=Math.Ceiling(Math.Sqrt(alphabet.Length));
            codealphabet = new string[(int)Math.Pow(a,2)];
            int i = 0;
            for(int it=0; it<a; it++)
            {
                for (int j = 0; j < a; j++)
                {
                    codealphabet[i] = alphabet[it] + alphabet[j];
                    i++;
                }

            }
        }
        private void Code(string[] alphabet1)
        {
            double a = Math.Ceiling(Math.Sqrt(alphabet1.Length));
            codealphabet = new string[(int)Math.Pow(a, 2)];
            int i = 0;
            for (int it = 0; it < a; it++)
            {
                for (int j = 0; j < a; j++)
                {
                    codealphabet[i] = alphabet[it] + alphabet[j];
                    i++;
                }

            }
        }
        public void addKey(string key)
       {
            string[] alphabet1 = new string[alphabet.Length];
            for(int i=0; i<key.Length; i++)
                for(int j=0; j< alphabet1.Length; j++)
                {
                    string a = "";
                    a+=key[i];
                    if (a == alphabet1[j])
                        alphabet1[j] = alphabet1[i];
                    alphabet1[i] = a;
                }
            Code(alphabet1);
       }
        public string Code(string text)
        {
            string text1 = "";
            foreach(var it in text)
            {
                string b = "";
                    b+=it;
                for (int i = 0; i < alphabet.Length; i++)
                    if (b == alphabet[i])
                        text1+= codealphabet[i];
            }
            StreamWriter sw = new StreamWriter("out.txt");
            sw.Write(text1);
            sw.Close(); 
            return text1;
        }
        public string Decode(string text)
        {
            string text1 = "";
            string[] text2 = new string[text.Length / 2];
            int j = 0;
            for (int i = 0; i < text.Length; i += 2) {
                text2[j] = text[i].ToString() + text[i + 1].ToString();
                    j++;
            }
            foreach (var it in text2)
            {
                for (int i = 0; i < codealphabet.Length; i++)
                    if (it == codealphabet[i])
                        text1+= alphabet[i];
            }
            return text1;
        }
    }
}
