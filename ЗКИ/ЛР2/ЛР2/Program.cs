﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;

namespace ЛР2
{
    class Program
    {
        static void Main(string[] args)
        {
            Polybius_square polybius_Square = new Polybius_square();
            bool flag = false;
            while (!flag)
            {
                Console.WriteLine("Имя файла с алфавитом");
                string filename = Console.ReadLine();
                if (File.Exists(filename))
                {
                    Alphabet alphabet = new Alphabet(filename);
                    polybius_Square = new Polybius_square(alphabet.Return());
                    flag = true;
                }
                else
                {
                    Console.WriteLine("Неправильное имя файла");
                }
                
            }
            Console.WriteLine("Введите кодовое слово");
            string cod = Console.ReadLine();
            polybius_Square.addKey(cod);
            Console.WriteLine("1. Зашифровать");
            Console.WriteLine("2. Расшифровать");
            string a = Console.ReadLine();
            switch (a)
            {
                case "1":
                    Console.WriteLine("Введите строку которую хотите зашифровать");
                    string text1 = Console.ReadLine();
                    Console.WriteLine(polybius_Square.Code(text1));
                    break;
                case "2":
                    Console.WriteLine("Введите строку которую хотите расшифровать");
                    string text2 = Console.ReadLine();
                    Console.WriteLine(polybius_Square.Decode(text2));
                    break;
                default: break;

            }
        }
    }
}
