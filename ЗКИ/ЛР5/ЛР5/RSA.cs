﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Speech;

namespace ЛР5
{
    class RSA
    {
        int p;
        int q;
        int n;
        int m;
        int d;
        int e;
        public RSA(string name)
        {
            string[] ward = File.ReadFile(name).Split();
            q = int.Parse(ward[2]);
            p = int.Parse(ward[1]);
            n = p * q;
            if (n < 9999)
                throw new MInExeption("Слишком маленькие p и q!");
            m = (p - 1) * (q - 1);
            Random random = new Random();
            d = random.Next(m + 1, int.MaxValue);
            while(NOD(m, d) != 1)
            {
                d++;
            }
            e = 1;
            while (e * d % m != 1)
                e++;
        }
        public RSA(string name1, string name2)
        {
            
        }
       public static  int NOD(int n1, int n2)
        {
            int div;
            if (n1 == n2)   // если числа равны, НОД найден
                return n1;
            int d = n1 - n2; // Находим разность чисел
            if (d < 0)       // если разность отрицательная,
            {
                d = -d;     // меняем знак
                div = NOD(n1, d); // вызываем функцию NOD() для двух наименьших чисел
            }
            else      // если разность n1-n2 положительная
            {
                div = NOD(n2, d); // вызываем функцию NOD() для двух наименьших чисел
            }
            return div;
        }
    }
}
