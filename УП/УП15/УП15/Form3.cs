﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
namespace УП15
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label6.Text = button1.Text;
            textBox1.Visible = false;
            textBox4.Visible = false;
            label5.Visible = false;
            label6.Visible = true;
            label4.Visible = false;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            if (label6.Text != "" && textBox2.Text != "" && textBox3.Text != "" /*&&*/ /*(textBox1.Text != "" && textBox1.Visible == true)*/)
                if (label6.Text == button1.Text)
                {
                    label4.Text = "Ответ: " + NonlinearEquation.Sin(textBox2.Text, textBox3.Text, out List<Point> points);
                    Form4 form4 = new Form4();
                 Grafic.Drawing(button1.Text, textBox2.Text, textBox3.Text, form4);
                    form4.Show();
                }
            if(label6.Text == "(x-")
            {
                label4.Text = "Ответ: " + NonlinearEquation.Ur2(textBox2.Text, textBox3.Text, Convert.ToInt32(textBox4.Text), Convert.ToInt32(textBox1.Text), out List<Point> points);
                Form4 form4 = new Form4();
                Grafic.Drawing1("x-b", textBox2.Text, textBox3.Text, form4, textBox4.Text, textBox1.Text);
                form4.Show();
            }
            if(label5.Text== "^x")
            {
                label4.Text = "Ответ: " + NonlinearEquation.a(textBox2.Text, textBox3.Text, Convert.ToInt32(textBox1.Text), out List<Point> points);
                Form4 form4 = new Form4();
                Grafic.Drawing1("a^x", textBox2.Text, textBox3.Text, form4, textBox1.Text);
                form4.Show();
            }
            label4.Visible = true;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            textBox1.Visible = true;
            textBox4.Visible = true;
            label5.Visible = true;
            label5.Text = ")^";
label6.Text = "(x-";
            label6.Visible = true;
            label4.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            textBox1.Visible = true;
            label5.Visible = true;
            textBox4.Visible = false;
            label6.Visible = false;
            label5.Text = "^x";
            label4.Visible = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            File.WriteFile("1.txt", label4.Text);
        }
    }
}
