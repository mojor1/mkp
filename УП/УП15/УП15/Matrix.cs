﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace УП15
{
    class Matrix
    {
       static DataGridView dataGridView = new DataGridView();
        static Label label = new Label();
        static public void AddMatrix(DataGridView dataGridView1, DataGridView dataGridView2, Form form, int x, int y)
        {
            dataGridView.Location = new Point(x, y);
            dataGridView.Parent = form;
            dataGridView.ColumnCount = dataGridView1.ColumnCount;
            dataGridView.RowCount = dataGridView1.RowCount;
            dataGridView.ColumnHeadersVisible = false;
            dataGridView.RowHeadersVisible = false;
            dataGridView.Size = dataGridView1.Size;

            for (int i = 0; i < dataGridView.ColumnCount; i++)
                for (int j = 0; j < dataGridView.RowCount; j++)
                    dataGridView[i, j].Value = (object)(Convert.ToInt32(dataGridView1[j, i].Value) + Convert.ToInt32(dataGridView2[i, j].Value));
                    form.Controls.Add(dataGridView);
        }
        static public void MulMatrix(DataGridView dataGridView1, DataGridView dataGridView2, Form form, int x, int y)
        {
            dataGridView.Location = new Point(x, y);
            dataGridView.Parent = form;
            dataGridView.ColumnCount = dataGridView1.RowCount;
            dataGridView.RowCount = dataGridView2.ColumnCount;
            dataGridView.ColumnHeadersVisible = false;
            dataGridView.RowHeadersVisible = false;
            dataGridView.Size = dataGridView1.Size;
            for (int i = 0; i < dataGridView1.RowCount; i++)
                for (int j = 0; j < dataGridView2.ColumnCount; j++)
                {
                    dataGridView[i, j].Value = 0;
                    for(int k=0; k< dataGridView1.ColumnCount; k++)
                    dataGridView[i, j].Value=(object)(Convert.ToInt32(dataGridView1[k, j].Value) * Convert.ToInt32(dataGridView2[i, k].Value)+ Convert.ToInt32(dataGridView[i, j].Value));
                }
            form.Controls.Add(dataGridView);
        }
        static public void RandomMatrix(DataGridView dataGridView1, int pred1, int pred2)
        {
            Random random = new Random();
            for (int i = 0; i < dataGridView1.ColumnCount; i++)
                for (int j = 0; j < dataGridView1.RowCount; j++)
                    dataGridView1[i, j].Value = random.Next(pred1, pred2);
        }
        static public int DetMatrix(DataGridView dataGridView1)
        {
            
            if (dataGridView1.ColumnCount == 2)
            {
                return Convert.ToInt32(dataGridView1[0, 0].Value) * Convert.ToInt32(dataGridView1[1, 1].Value) - Convert.ToInt32(dataGridView1[0, 1].Value) * Convert.ToInt32(dataGridView1[1, 0].Value);
            }
            int result = 0;
            dataGridView = dataGridView1;
            for (var j = 0; j < dataGridView1.ColumnCount; j++)
            {
                result += (j % 2 == 1 ? 1 : -1) * Convert.ToInt32(dataGridView1 [1,j].Value) * DetMatrix(CreateWithoutColumn(dataGridView, j));


            }
            return result;
        }
        static public DataGridView CreateWithoutColumn(DataGridView dataGridView1, int col)
        {
            DataGridView dataGridView2 = new DataGridView();
            dataGridView2.ColumnCount = dataGridView1.ColumnCount -1;
            dataGridView2.RowCount = dataGridView1.RowCount-1;
            int k = 0;
            for (int i = 0; i < dataGridView.ColumnCount; i++)
            {
                for (int j = 1; j < dataGridView.RowCount; j++)
                {
                    if(i!=col)
                    dataGridView2[k, j - 1].Value = dataGridView1[i, j].Value;
                }
                if(i!=col)
                k++;
            }
            return dataGridView2;
        }
    }
}
