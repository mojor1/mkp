﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace УП15
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }

        private void Form2_Load(object sender, EventArgs e)
        {
            dataGridView1.Visible = false;
            dataGridView2.Visible = false;
        }
        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "")
            {
                dataGridView1.ColumnCount = Convert.ToInt32(textBox1.Text);
                dataGridView1.RowCount = Convert.ToInt32(textBox2.Text);
                dataGridView1.Visible = true;
            }
            else dataGridView1.Visible = false;
        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "")
            {
                dataGridView1.ColumnCount = Convert.ToInt32(textBox1.Text);
                dataGridView1.RowCount = Convert.ToInt32(textBox2.Text);
                dataGridView1.Visible = true;
            }
            else dataGridView1.Visible = false;
        }
        int kol = 1;
        private void button4_Click(object sender, EventArgs e)
        {
            if (kol % 2 == 1)
            {
                label1.Visible = true;
                label2.Visible = true;
                textBox5.Visible = true;
                textBox6.Visible = true;

            }
            if (textBox5.Text != "" && textBox6.Text != "" && kol % 2 == 0)
            {
                try
                {
                    Matrix.RandomMatrix(dataGridView1, Convert.ToInt32(textBox6.Text), Convert.ToInt32(textBox5.Text));
                }
                catch
                {
                    MessageBox.Show("Минимальное значание больше максимального!");
                }
                label1.Visible = false;
                label2.Visible = false;
                textBox5.Visible = false;
                textBox6.Visible = false;
                kol = 0;
            }
            kol++;
        }
        int kol2 = 0;
        private void button1_Click(object sender, EventArgs e)
        {
            if (kol2 == 0)
            {
                dataGridView2.ColumnCount = dataGridView1.ColumnCount;
                dataGridView2.RowCount = dataGridView1.RowCount;
                dataGridView2.Visible = true;
                button5.Visible = true;
            }
            else
                Matrix.AddMatrix(dataGridView1, dataGridView2, this, 850, 12);
            kol2 = 1;
        }
        int kol1 = 1;
        private void button5_Click(object sender, EventArgs e)
        {
            if (kol1 % 2 == 1)
            {
                label3.Visible = true;
                label4.Visible = true;
                textBox4.Visible = true;
                textBox3.Visible = true;

            }
            if (textBox5.Text != "" && textBox6.Text != "" && kol1 % 2 == 0)
            {
                try
                {
                    Matrix.RandomMatrix(dataGridView2, Convert.ToInt32(textBox3.Text), Convert.ToInt32(textBox4.Text));
                }
                catch
                {
                    MessageBox.Show("Минимальное значание больше максимального!");
                }
                label3.Visible = false;
                label4.Visible = false;
                textBox3.Visible = false;
                textBox4.Visible = false;
                kol1 = 0;
            }
            kol1++;
        }
        int kol3 = 0;
        private void button2_Click(object sender, EventArgs e)
        {
            if (kol3 == 0)
            {
                dataGridView2.ColumnCount = dataGridView1.RowCount;
                dataGridView2.RowCount = dataGridView1.ColumnCount;
                dataGridView2.Visible = true;
                button5.Visible = true;
            }
            else
                Matrix.MulMatrix(dataGridView1, dataGridView2, this, 850, 12);
            kol3 = 1;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == textBox2.Text)
            {
                label6.Visible = true;
                label6.Text = Matrix.DetMatrix(dataGridView1).ToString();
            }
        }
    }
}
