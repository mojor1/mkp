﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;
using System.Windows.Forms;
using OxyPlot;
using OxyPlot.Series;

namespace УП15
{
    class Grafic
    {
        static public void Drawing(string name, string min, string max, Form4 form)
        {
            var myModel = new PlotModel { Title = name };
            Func<double, double> f =(x)=> Math.Sin(x);
            myModel.Series.Add(new FunctionSeries(f, int.Parse(min), int.Parse(max), 0.1, name));
            form.plotView1.Model = myModel;
        }
        static public void Drawing1(string name, string min, string max, Form4 form, string a, string b)
        {
            var myModel = new PlotModel { Title = name };
            Func<double, double> f = (x) => Math.Pow(x-int.Parse(b), int.Parse(a));
            myModel.Series.Add(new FunctionSeries(f, int.Parse(min), int.Parse(max), 0.1, name));
            form.plotView1.Model = myModel;
        }
        static public void Drawing1(string name, string min, string max, Form4 form, string a)
        {
            var myModel = new PlotModel { Title = name };
            Func<double, double> f = (x) => Math.Pow(int.Parse(a), x);
            myModel.Series.Add(new FunctionSeries(f, int.Parse(min), int.Parse(max), 0.1, name));
            form.plotView1.Model = myModel;
        }
        static public void Drawing2(string name, string min, string max, Form4 form)
        {
            var myModel = new PlotModel { Title = name };
            Func<double, double> f = (x) => -Math.Cos(x);
            myModel.Series.Add(new FunctionSeries(f, int.Parse(min), int.Parse(max), 0.1, name));
            form.plotView1.Model = myModel;
        }
        static public void Drawing2(string name, string min, string max, Form4 form, string a)
        {
            var myModel = new PlotModel { Title = name };
            Func<double, double> f = (x) => Math.Pow(x, int.Parse(a) + 1) / (int.Parse(a) + 1);
            myModel.Series.Add(new FunctionSeries(f, int.Parse(min), int.Parse(max), 0.1, name));
            form.plotView1.Model = myModel;
           
        }
        static public void Drawing3(string name, string min, string max, Form4 form, string a)
        {
            var myModel = new PlotModel { Title = name };
            Func<double, double> f = (x) => Math.Pow(int.Parse(a), x) / Math.Log(int.Parse(a));
            myModel.Series.Add(new FunctionSeries(f, int.Parse(min), int.Parse(max), 0.1, name));
            form.plotView1.Model = myModel;
        }
        static public void Drawing3(string name, string min, string max, Form4 form, string a, string b, string c, string d)
        {
            var myModel = new PlotModel { Title = name };
            Func<double, double> f = (x) => int.Parse(a) * Math.Pow(x, 4) / 4 + int.Parse(b) * Math.Pow(x, 3) / 3 + int.Parse(c) * Math.Pow(x, 2) / 2 + int.Parse(d) * x;
            myModel.Series.Add(new FunctionSeries(f, int.Parse(min), int.Parse(max), 0.1, name));
            form.plotView1.Model = myModel;
        }
    }
}
