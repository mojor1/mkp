﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OxyPlot;
using OxyPlot.Series;

namespace УП15
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }
        static int sumchis(int a)
        {
            int sum = 0;
            while (a >= 1)
            {
                sum += a % 10;
                a /= 10;
            }
            return sum;
        }
        static bool Shast(int a)
        {
            int b = a / 1000;
            int c = a % 1000;
            if (sumchis(b) == sumchis(c))
                return true;
            else return false;
        }
        
        private void Form4_Load(object sender, EventArgs e)
        {
            int kol = 0;
            List<int> bilet = new List<int>();
            Console.WriteLine("Введите количество билетов");
            int a = 100;
            Random random = new Random();
            chart1.DataSource = 10;
            chart1.Series.Add("procent");
            //chart1.Series[1].Points.AddY(100);
            for (int j = 0; j < 10; j++)
            {
                for (int i = 0; i < a; i++)
                    bilet.Add(random.Next(0, 1000000));
                foreach (int b in bilet)
                    if (Shast(b))
                        kol++;
                double procent = (double)kol / (double)bilet.Count * 100;
                chart1.Series[1].Points.AddY(procent);
               

            }

        }

        private void plotView1_Click(object sender, EventArgs e)
        {

        }
    }
}
