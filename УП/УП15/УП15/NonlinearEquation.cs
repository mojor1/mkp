﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace УП15
{
    class NonlinearEquation
    {
       static public string Sin(string min, string max, out List<Point> points)
        {
            string answer="";
            points = new List<Point>();
            int x1, x2;
            for (int i = Convert.ToInt32(min); i < Convert.ToInt32(max); i++)
            {
                x1 = (int)(2 * Math.PI * i);
                x2 = (int)(2 * Math.PI * i + Math.PI);
                points.Add(new Point(x1, x2));
                answer += $"[{x1}, {x2}]; ";
            }
            return answer;
        }
        static public string Ur2(string min, string max, int a, int b, out List<Point> points)
        {
            string answer = "";
            points = new List<Point>();
            if (a > 0 && Convert.ToInt32(max)>=b && Convert.ToInt32(min)<=b)
            {
                answer = b.ToString();
                int x, y;
                for (int i = Convert.ToInt32(min); i < Convert.ToInt32(max); i++)
                {
                    x = i;
                    y = (int)Math.Pow(x - b, a);
                    points.Add(new Point(x, y));
                }
            }
            else answer = "Корней нет!";
            return answer;
        }
        static public string a(string min, string max, int a, out List<Point> points)
        {
            string answer = "";
            points = new List<Point>();
            if (a == 0 && Convert.ToInt32(min)>0)
            {
                answer = "X принадлижит [1;+∞]";
                int x, y;
                for (int i = Convert.ToInt32(min); i < Convert.ToInt32(max); i++)
                {
                    x = i;
                    y = (int)Math.Pow(a, i);
                    points.Add(new Point(x, y));
                }
            }
            else answer = "Корней нет!";
            return answer;
        }
    }
}
