﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace УП15
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            label4.Visible = true;
            label4.Text = button1.Text;
            label1.Visible = false;
            label2.Visible = false;
            label3.Visible = false;
            textBox1.Visible = false;
            textBox2.Visible = false;
            textBox3.Visible = false;
            textBox4.Visible = false;
            button6.Visible = true;
            label5.Visible = false;

        }

        private void Form5_Load(object sender, EventArgs e)
        {
            label1.Visible = false;
            label2.Visible = false;
            label3.Visible = false;
            label4.Visible = false;
            textBox1.Visible = false;
            textBox2.Visible = false;
            textBox3.Visible = false;
            textBox4.Visible = false;

        }

        private void button2_Click(object sender, EventArgs e)
        {
            label3.Visible = true;
            textBox4.Visible = true;
            label3.Text = "x^";
            label1.Visible = false;
            label2.Visible = false;
            label4.Visible = false;
            textBox1.Visible = false;
            textBox2.Visible = false;
            textBox3.Visible = false;
            button6.Visible = true;
            label5.Visible = false;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            label4.Visible = true;
            label4.Text = button3.Text;
            label1.Visible = false;
            label2.Visible = false;
            label3.Visible = false;
            textBox1.Visible = false;
            textBox2.Visible = false;
            textBox3.Visible = false;
            textBox4.Visible = false;
            button6.Visible = true;
            label5.Visible = false;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            label4.Visible = true;
            textBox4.Visible = true;
            label4.Text = "^x";
            label1.Visible = false;
            label2.Visible = false;
            label3.Visible = false;
            textBox1.Visible = false;
            textBox2.Visible = false;
            textBox3.Visible = false;
            button6.Visible = true;
            label5.Visible = false;
        }

        private void button5_Click(object sender, EventArgs e)
        {
            label3.Visible = true;
            textBox4.Visible = true;
            label1.Visible = true;
            label2.Visible = true;
            label4.Visible = false;
            textBox1.Visible = true;
            textBox2.Visible = true;
            textBox3.Visible = true;
            button6.Visible = true;
            label1.Text = "x^3+";
            label2.Text = "x^2+";
            label3.Text = "x+";
            label5.Visible = false;
        }

        private void button6_Click(object sender, EventArgs e)
        {
            if(textBox5.Text!="" && textBox6.Text != "")
            {
                if (label4.Text == button1.Text && label4.Visible == true)
                {
                    label5.Text = (Math.Cos(int.Parse(textBox5.Text)) - Math.Cos(int.Parse(textBox6.Text))).ToString();
                    Form4 form4 = new Form4();
                    Grafic.Drawing2("-cos(x)", textBox5.Text, textBox6.Text, form4);
                    form4.Show();
                }
                if (label3.Text == "x^" && label3.Visible)
                {
                    label5.Text = (Math.Pow(int.Parse(textBox6.Text), int.Parse(textBox4.Text) + 1) / (int.Parse(textBox4.Text) + 1) - (Math.Pow(int.Parse(textBox5.Text), int.Parse(textBox4.Text) + 1) / (int.Parse(textBox4.Text) + 1))).ToString();
                    Form4 form4 = new Form4();
                    Grafic.Drawing2("x^(a+1)/(a+1)", textBox5.Text, textBox6.Text, form4, textBox4.Text);
                    form4.Show();
                }
                if (label4.Text == button3.Text && label4.Visible == true)
                {
                    label5.Text = (Math.Sin(int.Parse(textBox6.Text)) - Math.Sin(int.Parse(textBox5.Text))).ToString();
                    Form4 form4 = new Form4();
                    Grafic.Drawing("sin(x)", textBox5.Text, textBox6.Text, form4);
                    form4.Show();
                }
                if (label4.Text == "^x" && label4.Visible)
                {
                    label5.Text = (Math.Pow(int.Parse(textBox4.Text), int.Parse(textBox6.Text)) / Math.Log(int.Parse(textBox4.Text)) - (Math.Pow(int.Parse(textBox4.Text), int.Parse(textBox5.Text)) / Math.Log(int.Parse(textBox4.Text)))).ToString();
                    Form4 form4 = new Form4();
                    Grafic.Drawing3("a^x/ln(a)", textBox5.Text, textBox6.Text, form4, textBox4.Text);
                    form4.Show();
                }
                if (textBox1.Visible && textBox2.Visible && textBox3.Visible && textBox4.Visible)
                {
                    label5.Text = (int.Parse(textBox1.Text) * Math.Pow(int.Parse(textBox6.Text), 4) / 4 + int.Parse(textBox2.Text) * Math.Pow(int.Parse(textBox6.Text), 3) / 3 + int.Parse(textBox3.Text) * Math.Pow(int.Parse(textBox6.Text), 2) / 2 + int.Parse(textBox4.Text) * int.Parse(textBox6.Text) - (int.Parse(textBox1.Text) * Math.Pow(int.Parse(textBox5.Text), 4) / 4 + int.Parse(textBox2.Text) * Math.Pow(int.Parse(textBox5.Text), 3) / 3 + int.Parse(textBox3.Text) * Math.Pow(int.Parse(textBox5.Text), 2) / 2 + int.Parse(textBox4.Text) * int.Parse(textBox5.Text))).ToString();
                    Form4 form4 = new Form4();
                    Grafic.Drawing3("a*x^4/4+b*x^3/3+c*x^2/2+d*x", textBox5.Text, textBox6.Text, form4, textBox1.Text, textBox2.Text, textBox3.Text, textBox4.Text);
                    form4.Show();
                }
                }
            label5.Visible = true;
        }

        private void button7_Click(object sender, EventArgs e)
        {
            File.WriteFile("1.txt", label5.Text);
        }
    }
}
