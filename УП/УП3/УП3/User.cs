﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace УП3
{
    public class User
    {
       public string Name { get; set; }
       public string Login { get; set; }
       public string Password { get; set; }
       public string Role { get; set; }
       public List<DateTime> Dates { get; set; }
        public List<int> Procents { get; set; }
        public List<int> Time { get; set; }
        public User(string name, string login, string password, string role)
        {
            Name = name;
            Login = login;
            Password = password;
            Role = role;
            Dates = new List<DateTime>();
            Procents = new List<int>();
            Time = new List<int>();
        }
        public User(string name, string login, string password, string role, List<DateTime> dates, List<int> procents, List<int> time)
        {
            Name = name;
            Login = login;
            Password = password;
            Role = role;
            Dates = new List<DateTime>();
            Dates = dates;
            Procents = new List<int>();
            Procents = procents;
            Time = new List<int>();
            Time = time;
        }
        public User() {
            Dates = new List<DateTime>();
            Procents = new List<int>();
        }
        public void MakeTest(int procent)
        {
            if (Procents == null && Dates==null)
            {
                Procents = new List<int>();
                Dates = new List<DateTime>();
            }
            Dates.Add(DateTime.Now);
            Procents.Add(procent);    
        }
        public bool Proverka(string login, string password)
        {
            if (Login == login && Password==password)
                return true;
            else
                return false;
        }
        public void NewPassword(string password, string newpassword)
        {
            if (password == this.Password)
            {
                Password = newpassword;
            }
        }

    }
}
