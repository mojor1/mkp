﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace УП3
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        public string text { get; set; }
        Questions questions = new Questions();
        private void Form2_Load(object sender, EventArgs e)
        {
            questions.GetData();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            timer1.Start();
            questions.Generate(this);
            button1.Visible = false;
            button2.Visible = false;
            textBox1.Visible = false;
            textBox2.Visible = false;
            label2.Visible = false;
            label3.Visible = false;
        }

        public void Label7_Click(object sender, EventArgs e)
        {
            questions.Label1_Click(this, 6, text);
        }

        public void Label6_Click(object sender, EventArgs e)
        {

            questions.Label1_Click(this, 5, text);
        }

        public void Label5_Click(object sender, EventArgs e)
        {
            questions.Label1_Click(this, 4, text);
        }

        public void Label4_Click(object sender, EventArgs e)
        {
            questions.Label1_Click(this, 3, text);
        }

        public void Label3_Click(object sender, EventArgs e)
        { 
            questions.Label1_Click(this, 2, text);
        }

        public void Label2_Click(object sender, EventArgs e)
        {
            questions.Label1_Click(this, 1, text);
        }

        public void Label1_Click(object sender, EventArgs e)
        {
            questions.Label1_Click(this, 0, text);
        }
        public void Button_Click(object sender, EventArgs e)
        {
            questions.Prev(this);
        }

        private void Form2_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form1 form1 = new Form1();
            form1.Show();
        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (this.textBox1.Text != "" && this.textBox2.Text != "")
            {
                Users users = new Users();
                users.GetData();
                users.NewPassword(text, this.textBox1.Text, this.textBox2.Text);
                users.LondingData();
                textBox1.Clear();
                textBox2.Clear();
            }
            else MessageBox.Show("Не введён старый пароль или новый пароль!");
        }
    }
}
