﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace УП3
{
   public  class Questions : IEnumerable<Question>
    {
        public List<Question> Questions1 { get; set; }
        private int kol = 0;
        List<Label> label1;
        Label label = new Label();
        Button button2=new Button();
        List<int> right = new List<int>();
        int date = DateTime.Now.Second + DateTime.Now.Minute*60;
        bool flag = true;
        public Questions()
        {
            Questions1 = new List<Question>();
        }
        public Questions(List<Question> questions)
        {
            Questions1 = questions;
        }
        public Questions(Question question)
        {
            Questions1 = new List<Question>();
            Questions1.Add(question);
        }
        public void Add(Question question)
        {
            Questions1.Add(question);
        }
        public void Generate(Form2 form)
        {
            if (kol >= Questions1.Count)
            {
                Console.WriteLine("Вы генирируете больше вопросов чем есть!");
            }
            else
            {
                if (flag == true)
                {
                    date = DateTime.Now.Second+DateTime.Now.Minute * 60;
                    flag = false;
                }
                if (kol > 0)
                {
                    button2 = new Button();
                    button2.Location = new Point(300, 300);
                    button2.Text = "Назад";
                    button2.Parent = form;
                    button2.Click += new EventHandler(form.Button_Click);
                    form.Controls.Add(button2);
                }
                List<int> vs1 = new List<int>();
                label = new Label();
                label.Text = Questions1.ToArray()[kol].Questiont;
                label.Location = new Point(20, 10);
                label.Visible = true;
                label.AutoSize = true;
                label.Parent = form;
                form.Controls.Add(label);
                System.EventHandler[] vs = { form.Label1_Click, form.Label2_Click, form.Label3_Click, form.Label4_Click, form.Label5_Click, form.Label6_Click, form.Label7_Click };
                while (vs1.Count != Questions1.ToArray()[kol].Answers.Count)
                {
                    bool flag = false;
                    Random random = new Random();
                    int a = random.Next(0, Questions1.ToArray()[kol].Answers.Count);
                    foreach (var it in vs1)
                        if (a == it)
                            flag = true;
                    if (!flag)
                        vs1.Add(a);
                }
                label1 = new List<Label>();
                for (int i = 0; i < Questions1.ToArray()[kol].Answers.Count; i++)
                    label1.Add(new Label());
                for (int i = 0; i < label1.Count; i++)
                {
                    label1.ToArray()[i].Click += vs[i];
                    label1.ToArray()[i].Location = new Point(20, 40 + 40 * i);
                    label1.ToArray()[i].Visible = true;
                    label1.ToArray()[i].Parent = form;
                    form.Controls.Add(label1.ToArray()[i]);
                }
                for (int i = 0; i < label1.Count; i++)
                    label1.ToArray()[i].Text = Questions1.ToArray()[kol].Answers.ToArray()[vs1.ToArray()[i]];
                kol++;
            }
            
        }
        public void Label1_Click(Form2 form, int i, string text)
        {
            if (label1.ToArray()[i].Text == Questions1.ToArray()[kol - 1].Answers.ToArray()[0])
                right.Add(1);
            else right.Add(0);
            this.Distruct();
            if (kol != Questions1.Count)
            {
                this.Generate(form);
            }
            else this.Result(form, text);
        }
        public void Distruct()
        {
            button2.Dispose();
            label.Dispose();
            foreach (var it in label1)
                it.Dispose();
            label1.Clear();
        }
        public void Result(Form2 form, string text)
        {
            Users users = new Users();
            users.GetData();
            double summ=0;
            int date1 = DateTime.Now.Second + DateTime.Now.Minute * 60;
            date1 -= date;
            foreach (int it in right)
                summ += it;
            label = new Label();
            label.Text = "Ваш результат:"+(summ / kol*100).ToString()+"% за "+date1+" Секунд";
            label.Location = new Point(60, 60);
            label.Visible = true;
            label.AutoSize = true;
            label.Parent = form;
            form.Controls.Add(label);
           
            foreach(User user in users)
            {
                if (user.Login == text)
                {
                    user.Dates.Add(DateTime.Now.Date);
                    user.Procents.Add((int)(summ / kol * 100));
                    if(user.Time==null)
                    user.Time = new List<int>();
                    user.Time.Add(date1);
                }
            }
            users.LondingData();
        }
        public void Prev(Form2 form2)
        {
            kol-=2;
            right.Remove(right.Count - 1);
            this.Distruct();
            this.Generate(form2);

        }
        public void GetData()
        {
            Questions1=File.GetQussions();
        }
        public void LondingData()
        {
             File.LondingQuestions(Questions1);
        }

        public IEnumerator<Question> GetEnumerator()
        {
            return Questions1.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
