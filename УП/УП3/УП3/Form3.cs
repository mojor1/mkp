﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace УП3
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
        }

        private void Form3_Load(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "") {
                Users users = new Users();
                users.GetData();
                if (users.AddUser(textBox1.Text, textBox2.Text, textBox3.Text, "user"))
                {
                    users.LondingData();
                    Form1 form1 = new Form1();
                    form1.Show();
                    this.Close();
                }
                else MessageBox.Show("Данный логин уже занят!");
             }
            else MessageBox.Show("Вы заполнили не все поля!");

        }

        private void Form3_Shown(object sender, EventArgs e)
        {
            textBox1.Clear();
            textBox2.Clear();
            textBox3.Clear();
        }
    }
}
