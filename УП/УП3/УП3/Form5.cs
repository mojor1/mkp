﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace УП3
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
        }

        private void Form5_Load(object sender, EventArgs e)
        {
            Users users = new Users();
            users.GetData();
            
            for(int i=0; i<users.ToArray().Length; i++)
            {
                dataGridView1.Rows.Add();
                for (int j = 0; j < dataGridView1.ColumnCount; j++) {
                    if (j == 0)
                        dataGridView1[j, i].Value = users.ToArray()[i].Login;
                    if (j == 1)
                    {
                        User user = users.ToArray()[i];
                        foreach (DateTime it in user.Dates)
                        {
                            dataGridView1[j, i].Value += it.ToString("d") + ";  ";
                        }
                    }
                    if (j == 2)
                    {
                        User user = users.ToArray()[i];
                        foreach (int it in user.Procents)
                        {
                            dataGridView1[j, i].Value += it.ToString() + "%  ";
                        }
                    }
                    if (j == 3)
                    {
                        User user = users.ToArray()[i];
                        if(user.Time!=null)
                        foreach (int it in user.Time)
                        {
                            dataGridView1[j, i].Value += it.ToString() + "секунд  ";
                        }
                    }
                }
            }
            dataGridView1.AutoSize = true;
        }

        private void Form5_FormClosing(object sender, FormClosingEventArgs e)
        {
            
        }

        private void Form5_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form1 form1 = new Form1();
            form1.Show();
        }
    }
}
