﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Text;
using System.Threading;
using System.Configuration;
using System.Linq;

namespace УП3
{
   public class File
    {
        //string Filename { get; set; }
        //public File(string filename)
        //{
        //    Filename = filename;
        //}
        public static void LondingData(List<User> users)
        {
            XmlDocument newXDoc = new XmlDocument();
            XmlElement newXRoot = newXDoc.CreateElement("users");
            foreach (var user in users)
            {
                XmlElement xmlUserElement = newXDoc.CreateElement("user");
                XmlElement LoginElem = newXDoc.CreateElement("login");
                XmlElement PasswordElem = newXDoc.CreateElement("password");
                XmlElement Dates = newXDoc.CreateElement("dates");
                XmlElement Role = newXDoc.CreateElement("role");
                XmlElement Procents = newXDoc.CreateElement("procents");
                XmlElement Time = newXDoc.CreateElement("time");
                XmlText xmlAgeValue = newXDoc.CreateTextNode(user.Login);
                String datase="";
                foreach(DateTime it in user.Dates)
                {
                    datase += it.ToString("d")+";";
                }
                string procents = "";
                foreach (int it in user.Procents)
                {
                    procents += it.ToString()+";";
                }
                string time = "";
                if(user.Time!=null)
                foreach (int it in user.Time)
                {
                    time += it.ToString() + ";";
                }
                XmlText xmlCompanyValue = newXDoc.CreateTextNode(user.Password);
                XmlText xmlPincodValue = newXDoc.CreateTextNode(datase);
                XmlText xmlRoleValue = newXDoc.CreateTextNode(user.Role);
                XmlText xmlProcentsValue = newXDoc.CreateTextNode(procents);
                XmlText xmlTimeValue = newXDoc.CreateTextNode(time);
                LoginElem.AppendChild(xmlAgeValue);
                PasswordElem.AppendChild(xmlCompanyValue);
                Dates.AppendChild(xmlPincodValue);
                Role.AppendChild(xmlRoleValue);
                Procents.AppendChild(xmlProcentsValue);
                Time.AppendChild(xmlTimeValue);
                xmlUserElement.SetAttribute("name", user.Name);
                xmlUserElement.AppendChild(LoginElem);
                xmlUserElement.AppendChild(PasswordElem);
                xmlUserElement.AppendChild(Dates);
                xmlUserElement.AppendChild(Role);
                xmlUserElement.AppendChild(Procents);
                xmlUserElement.AppendChild(Time);
                newXRoot.AppendChild(xmlUserElement);
            }

            newXDoc.AppendChild(newXRoot);
            newXDoc.Save("D://MKP//users.xml");
        }
        public static void LondingQuestions(List<Question> questions)
        {
            XmlDocument newXDoc = new XmlDocument();
            XmlElement newXRoot = newXDoc.CreateElement("questions");
            foreach (Question question in questions)
            {
                XmlElement xmlUserElement = newXDoc.CreateElement("question");
                XmlElement Answer1 = newXDoc.CreateElement("answer1");
                XmlElement Answer2 = newXDoc.CreateElement("answer2");
                XmlElement Answer3 = newXDoc.CreateElement("answer3");
                XmlElement Answer4 = newXDoc.CreateElement("answer4");
                XmlElement Answer5 = newXDoc.CreateElement("answer5");
                XmlElement Answer6 = newXDoc.CreateElement("answer6");
                XmlElement Answer7 = newXDoc.CreateElement("answer7");
                int i = 0;
                XmlText xmlCompanyValue = newXDoc.CreateTextNode(question.Answers.ToArray()[i]);
                i++;
                XmlText xmlPincodValue = newXDoc.CreateTextNode(question.Answers.ToArray()[i]);
                i++;
                XmlText xmlRoleValue = newXDoc.CreateTextNode(question.Answers.ToArray()[i]);
                i++;
                XmlText xmlProcentsValue;
                if (i < question.Answers.Count)
                {
                    xmlProcentsValue = newXDoc.CreateTextNode(question.Answers.ToArray()[i]);
                    i++;
                }
                else  xmlProcentsValue = newXDoc.CreateTextNode(" ");
                XmlText xmlAnswer5;
                if (i < question.Answers.Count)
                {
                     xmlAnswer5 = newXDoc.CreateTextNode(question.Answers.ToArray()[i]);
                    i++;
                }
                else xmlAnswer5 = newXDoc.CreateTextNode(" ");
                XmlText xmlAnswer6;
                if (i < question.Answers.Count)
                {
                    xmlAnswer6 = newXDoc.CreateTextNode(question.Answers.ToArray()[i]);
                    i++;
                }
                else xmlAnswer6 = newXDoc.CreateTextNode(" ");
                XmlText xmlAnswer7;
                if (i < question.Answers.Count)
                {
                    xmlAnswer7 = newXDoc.CreateTextNode(question.Answers.ToArray()[i]);
                    i++;
                }
                else xmlAnswer7 = newXDoc.CreateTextNode(" ");
                Answer1.AppendChild(xmlCompanyValue);
                Answer2.AppendChild(xmlPincodValue);
                Answer3.AppendChild(xmlRoleValue);
                Answer4.AppendChild(xmlProcentsValue);
                Answer5.AppendChild(xmlAnswer5);
                Answer6.AppendChild(xmlAnswer6);
                Answer7.AppendChild(xmlAnswer7);
                xmlUserElement.SetAttribute("question", question.Questiont);
                xmlUserElement.AppendChild(Answer1);
                xmlUserElement.AppendChild(Answer2);
                xmlUserElement.AppendChild(Answer3);
                xmlUserElement.AppendChild(Answer4);
                xmlUserElement.AppendChild(Answer5);
                xmlUserElement.AppendChild(Answer6);
                xmlUserElement.AppendChild(Answer7);
                newXRoot.AppendChild(xmlUserElement);
            }

            newXDoc.AppendChild(newXRoot);
            newXDoc.Save("D://MKP//question.xml");
        }
        public static List<User> GetData()
        {
            List<User> users = new List<User>();
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load("D://MKP//users.xml");
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlElement xnode in xRoot)
            {
                User user = new User();
                XmlNode attr = xnode.Attributes.GetNamedItem("name");
                if (attr != null)
                    user.Name = attr.Value;
                foreach (XmlNode childnode in xnode.ChildNodes)
                {
                    if (childnode.Name == "login")
                        user.Login = childnode.InnerText;
                    if (childnode.Name == "password")
                       user.Password= childnode.InnerText;
                    if (childnode.Name == "role")
                        user.Role = childnode.InnerText;
                    if(childnode.Name == "dates")
                    {
                        if (childnode.InnerText != "")
                        {
                            string[] vs = childnode.InnerText.Split(';');
                            foreach (string it in vs)
                            {
                                if (user.Dates == null)
                                    user.Dates = new List<DateTime>();
                                if(it!=vs.Last())
                                user.Dates.Add(Convert.ToDateTime(it));
                            }
                        }
                    }
                    if (childnode.Name == "procents")
                    {
                        if (childnode.InnerText != "")
                        {
                            string[] vs = childnode.InnerText.Split(';');
                            foreach (string it in vs)
                            {
                                if (user.Procents == null)
                                    user.Procents = new List<int>();
                                if (it != vs.Last())
                                    user.Procents.Add(Convert.ToInt32(it));
                            }
                        }
                    }
                    if (childnode.Name == "time")
                    {
                        if (childnode.InnerText != "")
                        {
                            string[] vs = childnode.InnerText.Split(';');
                            foreach (string it in vs)
                            {
                                if (user.Time == null)
                                    user.Time = new List<int>();
                                if (it != vs.Last())
                                    user.Time.Add(Convert.ToInt32(it));
                            }
                        }
                    }
                }
                users.Add(user);
            }

            return users;
        }
        public static List<Question> GetQussions()
        {
            List<Question> questions = new List<Question>();
            XmlDocument xDoc = new XmlDocument();
            xDoc.Load("D://MKP//question.xml");
            XmlElement xRoot = xDoc.DocumentElement;
            foreach (XmlElement xnode in xRoot)
            {
                Question question = new Question();
                XmlNode attr = xnode.Attributes.GetNamedItem("question");
                if (attr != null)
                    question.Questiont = attr.Value;
                foreach (XmlNode childnode in xnode.ChildNodes)
                {
                    if (childnode.Name == "answer1")
                        question.AddAnswer(childnode.InnerText);
                    if (childnode.Name == "answer2")
                        question.AddAnswer(childnode.InnerText);
                    if (childnode.Name == "answer3")
                        question.AddAnswer(childnode.InnerText);
                    if (childnode.Name == "answer4")
                        question.AddAnswer(childnode.InnerText);
                    if (childnode.Name == "answer5")
                        question.AddAnswer(childnode.InnerText);
                    if (childnode.Name == "answer6")
                        question.AddAnswer(childnode.InnerText);
                    if (childnode.Name == "answer7")
                        question.AddAnswer(childnode.InnerText);
                }
                questions.Add(question);
            }
            return questions;
        }
   }
}
