﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace УП3
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
        }
       
        List<Label> labels = new List<Label>();
        private void button3_Click(object sender, EventArgs e)
        {
            Questions questions = new Questions();
            questions.GetData();
            int i = 0;
            foreach(Question it in questions)
            {
                Label label = new Label();
                label.Text = it.Questiont;
                label.Location = new Point(350, 60+10*i);
                label.Visible = true;
                label.Click += new EventHandler(Label_Click);
                label.AutoSize = true;
                Controls.Add(label);
                labels.Add(label);
                i++;
            }

        }

        private void Label_Click(object sender, EventArgs e)
        {
            Questions questions = new Questions();
            questions.GetData();
            foreach (Question it in questions)
            {
                if (((Label)sender).Text == it.Questiont)
                {
                    questions.Questions1.Remove(it);
                    break;
                }
            }
            questions.LondingData();
            foreach (var it in labels)
                it.Dispose();
            labels.Clear();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            foreach (var it in labels)
                it.Dispose();
            labels.Clear();
            label4.Visible = true;
            label5.Visible = true;
            textBox4.Visible = true;
            textBox5.Visible = true;
            textBox6.Visible = true;
            textBox7.Visible = true;
            textBox8.Visible = true;
            textBox9.Visible = true;
            textBox10.Visible = true;
            textBox11.Visible = true;
            label6.Visible = true;
            label7.Visible = true;
            label8.Visible = true;
            label9.Visible = true;
            label10.Visible = true;
            label11.Visible = true;
            button4.Visible = true;
        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void textBox9_TextChanged(object sender, EventArgs e)
        {

        }

        private void button2_Click(object sender, EventArgs e)
        {
            if (textBox1.Text != "" && textBox2.Text != "" && textBox3.Text != "")
            {
                Users users = new Users();
                users.GetData();
                if (users.AddUser(textBox1.Text, textBox2.Text, textBox3.Text, "teacher"))
                {
                    users.LondingData();
                    this.Close();
                }
                else MessageBox.Show("Данный логин уже занят!");
            }
            else MessageBox.Show("Вы заполнили не все поля!");
        }

        private void button4_Click(object sender, EventArgs e)
        {
            if (textBox4.Text != "" && textBox5.Text != "" && textBox6.Text != "" && textBox8.Text != "")
            {
                Question question = new Question(textBox6.Text, textBox5.Text, textBox4.Text, textBox8.Text);
                if (textBox7.Text != "")
                    question.AddAnswer(textBox7.Text);
                if (textBox9.Text != "")
                    question.AddAnswer(textBox9.Text);
                if (textBox10.Text != "")
                    question.AddAnswer(textBox10.Text);
                if (textBox11.Text != "")
                    question.AddAnswer(textBox11.Text);
                Questions questions = new Questions();
                questions.GetData();
                questions.Questions1.Add(question);
                questions.LondingData();
                label4.Visible = false;
                label5.Visible = false;
                textBox4.Visible = false;
                textBox5.Visible = false;
                textBox6.Visible = false;
                textBox7.Visible = false;
                textBox8.Visible = false;
                textBox9.Visible = false;
                textBox10.Visible = false;
                textBox11.Visible = false;
                label6.Visible = false;
                label7.Visible = false;
                label8.Visible = false;
                label9.Visible = false;
                label10.Visible = false;
                label11.Visible = false;
                button4.Visible = false;
            } else MessageBox.Show("Вы заполнили не все поля!");

        }

        private void Form4_FormClosed(object sender, FormClosedEventArgs e)
        {
            Form1 form1 = new Form1();
            form1.Show();
        }
    }
}
