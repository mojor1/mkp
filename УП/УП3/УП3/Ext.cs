﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace УП3
{
    public static class Ext
    {
        public static T DeepCopy<T>(T obj)
        {
            if (ReferenceEquals(obj, null)) return default;
            if (!typeof(T).IsSerializable) throw new ArgumentException("obj must be serializable");
            var formatter = new BinaryFormatter();
            using (var stream = new MemoryStream())
            {
                formatter.Serialize(stream, obj);
                var newobj = (T)formatter.Deserialize(stream);
                return newobj;
            }
        }
    }
}
