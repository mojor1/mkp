﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace УП3
{
   public class Users : IEnumerable<User>
    {
        public List<User> users = new List<User>();
        public Users(User user)
        {
            users = new List<User>();
            users.Add(user);
        }
        public Users()
        {
            users = new List<User>();
        }
        public bool AddUser(User user)
        {
            if (users == null)
                users = new List<User>();
            bool flag = false;
            foreach (var it in users)
                if (it.Login == user.Login)
                    flag = true;
            if (!flag)
            {
                users.Add(user);
                return true;
            }
            else return false;
        }
        public bool AddUser(string name, string login, string password, string role, List<DateTime> dates, List<int> procents, List<int> times)
        { 
                User user = new User(name, login, password, role, dates, procents, times);
               return this.AddUser(user);
        }
        public bool AddUser(string name, string login, string password, string role)
        {
            User user = new User(name, login, password, role);
           return this.AddUser(user);
        }
        public bool Proverka(string login, string password, out string role)
        {
            role = "";
            bool flag = true;
            foreach (var it in users)
                if (it.Proverka(login, password))
                {
                    flag = true;
                    role = it.Role;
                }
            if (!flag)
                return false;
            else return true;
        }
        public void GetData()
        {
            users = File.GetData();
        }
        public void LondingData()
        {
            File.LondingData(users);
        }
        public void NewPassword(string login, string password, string newpassword)
        {
            foreach(User user in users)
            {
                if (user.Login == login)
                    user.NewPassword(password, newpassword);
            }
        }
        public IEnumerator<User> GetEnumerator()
        {
            return users.GetEnumerator();
        }

        IEnumerator IEnumerable.GetEnumerator()
        {
            return GetEnumerator();
        }
    }
}
