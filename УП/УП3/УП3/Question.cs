﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace УП3
{
   public class Question
    {
        public string Questiont { get; set; }
        public List<string> Answers { get; set;}
        public Question()
        {
            Answers = new List<string>();
        }
        public Question(string question, List<string> answers)
        {
            if (answers.Count > 7)
                throw new Exception();
            else
            {
                Questiont = question;
                Answers = new List<string>();
                Answers = answers;
            }
        }
        public Question(string question, string answer1, string answer2, string answer3)
        {
            Questiont = question;
            Answers = new List<string>();
            Answers.Add(answer1);
            Answers.Add(answer2);
            Answers.Add(answer3);
        }
        public void AddAnswer(string answer)
        {
            if (Answers.Count > 6)
                throw new Exception();
            else if(answer!="")
            {
                Answers.Add(answer);
            }
        }
   }
}
