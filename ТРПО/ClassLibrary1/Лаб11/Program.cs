﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ClassLibrary1;

namespace Лаб11
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.Write("Первое слово: ");
            var s1 = Console.ReadLine();
            Console.Write("Второе слово: ");
            var s2 = Console.ReadLine();

            Console.WriteLine("Расстояние Левенштейна: {0}", Levenshtein.LevenshteinDistance(s1, s2));
            try
            {
                Console.WriteLine("Введите максимальное расстояние Левенштейна");
                var a = int.Parse(Console.ReadLine());
                if (Levenshtein.DamerauLevenshteinDistance(s1, s2, a) == int.MaxValue)
                    Console.WriteLine("Строки не совпадают");
                else
                    Console.WriteLine("Расстояние Левенштейна: {0}", Levenshtein.DamerauLevenshteinDistance(s1, s2, 6));
            }
            catch
            {
                Console.WriteLine("Вы ввели не целое число значение");
            }
            
            Console.ReadLine();
        }
    }
}
