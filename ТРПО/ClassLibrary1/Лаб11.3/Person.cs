﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Лаб11._3
{
    class Person
    {
        
        public string name;

        public Person(string name, int age, string adress)
        {
            this.name = name;
            Age = age;
            Adress = adress;
        }
        [Atribut]
        public int? Age { get; set; }
        public string Adress { get; set; }
        public override string ToString()
        {
            
            return $"Имя: {name}, Адрес: {Adress}, Возвраст: {Age}";
        }

        public static string b() => "b";
    }
}
