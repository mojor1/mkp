﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Лаб11._3
{
    class Program
    {
        static void Main(string[] args)
        {
            Atribut atribut = new Atribut();
            Console.WriteLine("Свойство с атрибутом:");
            Type myType = Type.GetType("Лаб11._3.Person", false, true);
            foreach (MemberInfo mi in myType.GetMembers())
            {
                if(Attribute.IsDefined(mi, atribut.GetType()))
                Console.WriteLine($"{mi.DeclaringType} {mi.MemberType} {mi.Name}");
            }
            Console.WriteLine("-------");
            foreach (MemberInfo mi in myType.GetMembers())
            {
                    Console.WriteLine($"{mi.DeclaringType} {mi.MemberType} {mi.Name}");
            }
            Console.WriteLine("Вызов функции:");
            Console.WriteLine(myType.GetMethod("b").Invoke(null, null));
            
        }
    }
}
