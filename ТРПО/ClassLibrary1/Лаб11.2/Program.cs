﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Лаб11._2
{
    class Program
    {
        public delegate void Arifm(int a, double b);
        static public void Sum(int a, double b)
        {
            Console.WriteLine(a + b); 
        }
        static public double Sum1(int a, double b)
        {
            return a + b;
        }
        static public void Mul(int a, double b)
        {
            Console.WriteLine(a * b);
        }
        static public void Div(int a, double b)
        {
            Console.WriteLine(a / b);
        }
        static public void Minus(int a, double b)
        {
            Console.WriteLine(a - b);
        }
        static public void Math(int a, double b, Arifm arifm)
        {
             arifm(a, b);
        }
        static public void Math(int a, double b, Action<int, double> arifm)
        {
            arifm(a, b);
        }
        static void Main(string[] args)
        {
            Action<int, double> a;
            Func<int, double, double> Func;
            Func = Sum1;
            Arifm arifm = Sum;
            Math(5,6,arifm);
            a = Sum;
            Math(5, 6, a);
             arifm = Mul;
            Math(5, 6, arifm);
            a = Mul;
            Math(5, 6, a);
            arifm = Div;
           Math(5, 6, arifm);
            a = Div;
            Math(5, 6, a);
            arifm = Minus;
            Math(5, 6, arifm);
            a = Minus;
            Math(5, 6, a);
            arifm = (x, y) => Console.WriteLine(x + y); 
            Math(5, 6, arifm);
            a = (x, y) => Console.WriteLine(x + y);
            Math(5, 6, a);
        }
    }
}
