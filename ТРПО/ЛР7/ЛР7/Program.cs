﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР7
{
    class Program
    {
        static void Main(string[] args)
        {
            List<Card> cards = new List<Card>();
            cards.Add(new Card(12345, 2546));
            cards.Add(new Card(56789, 2646));
            cards.Add(new Card(54321, 2746));
            ATM atm = new ATM(new Vaiting(), 1, 32000, cards);
            atm.Function(12345);
            atm.Function(2546);
            atm.Function(32000);
            atm.Function(32000);

            Console.Read();
        }
    }
    struct Card
    {
       public int pincod;
        public int number_of_card;

        public Card(int number_of_card, int pincod)
        {
            this.pincod = pincod;
            this.number_of_card = number_of_card;
        }
    }
    class ATM
    {
        int ID;
        public double sum;
        public List<Card> cards= new List<Card>();
        public IATMState State { get; set; }
        public const double ver_of_lock_of_communication = 0.05;
        public int card_number;
        public ATM(IATMState ws, int id, double sum, List<Card> pincod)
        {
            ID = id;
            this.sum = sum;
            this.cards = pincod;
            State = ws;
        }
        public bool AddCard_Number(int card_number)
        {
            bool flag = false;
            foreach (Card card in cards)
                if (card_number == card.number_of_card)
                {
                    flag = true;
                    this.card_number = card_number;
                }
            return flag;
        }
        public void Function(double vvod)
        {
            PrintState();
           double a= State.Function(this, vvod);
            if(a!=0)
                Console.WriteLine($"Вы сняли {a} денег");
        }
        public void PrintState()
        {
            State.PrintState(this);
        }
    }

    interface IATMState
    {
        void PrintState(ATM water);
        double Function(ATM atm, double vvod);

    }
    class Authentication : IATMState
    {
        public double Function(ATM atm, double vvod)
        {
            try
            {
                bool flag = false;
                foreach (Card card in atm.cards)
                    if (card.pincod == int.Parse(vvod.ToString()) && card.number_of_card == atm.card_number)
                    {
                        atm.State = new WorkState();
                        flag = true;
                    }
                if(!flag)
                    Console.WriteLine("Введён неверный пароль!");
                return 0;
            }
            catch
            {
                Console.WriteLine("Введён неверный пароль!");
                return 0;
            }
        }


        void IATMState.PrintState(ATM atm)
        {
            Console.WriteLine("Идёт аутификация!");
        }
    }
    class WorkState : IATMState
    {
        public double Function(ATM atm, double vvod)
        {
            if (atm.sum >= vvod)
            {
                atm.sum -= vvod;
                if (atm.sum == 0)
                    atm.State = new NoMony();
                else
                atm.State = new Vaiting();
                return vvod;
            }
            else { Console.WriteLine("Такого количества денег в банкомате нет!"); return 0; }
           
        }

        void IATMState.PrintState(ATM atm)
        {
            Console.WriteLine("Идёт выполнение операций");
        }
    }
    class NoMony : IATMState
    {
        public double Function(ATM atm, double vvod)
        {
            atm.sum += vvod;
            atm.State = new Vaiting();
            atm.PrintState();
            return 0;
            
        }

         void IATMState.PrintState(ATM water)
        {
            Console.WriteLine("Банкомат заблокирован. Денег нет.");
        }
    }
    class Vaiting : IATMState
    {
        public double Function(ATM atm, double vvod)
        {
            try
            {
                if (atm.AddCard_Number(int.Parse(vvod.ToString())))
                {
                    atm.State = new Authentication();
                    return 0;
                }
                else { Console.WriteLine("Такой карты не существует!"); return 0; }
            }
            catch
            {
                Console.WriteLine("Такой карты не существует!"); return 0;
            }
        }

        void IATMState.PrintState(ATM water)
        {
            Console.WriteLine("Ожидаем вставки карты");
        }
    }
}
