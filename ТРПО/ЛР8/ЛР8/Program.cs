﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GRASP;

namespace ЛР8
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Создаём цех. Введите адресс цеха");
            WorkShop workShop = new WorkShop(Console.ReadLine());
            bool flag = true;
            while (flag)
            {
                Console.WriteLine("1.Добавить рабочего в цех");
                Console.WriteLine("2.Добавить станок для обслуживания рабочему");
                Console.WriteLine("3.Вывести информацию о цехе");
                Console.WriteLine("4.Вывести информацию о работниках");
                Console.WriteLine("5.Сумарные затраты на заработную плату сотрудникам");
                Console.WriteLine("6.Заработная плата определённого сотрудника");
                Console.WriteLine("7.Выход");
                string a = Console.ReadLine();
                switch (a)
                {
                    case "1":
                        try
                        {
                            Console.WriteLine("Введите ФИО работника, ID работника и ID стонка который он будет обслуживать (Все величины вводить через энтер)");
                            workShop.AddWorker(Console.ReadLine(), int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()));
                        }
                        catch (ExistExeption exeption)
                        {
                            Console.WriteLine(exeption);
                        }
                        catch
                        {
                            Console.WriteLine("Что-то пошло не так");
                        }
                        break;
                    case "2":
                        try
                        {
                            Console.WriteLine("ID работника и ID стонка который он будет обслуживать (Все величины вводить через энтер)");
                            workShop.AddMachine(int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()));
                        }
                        catch (ExistExeption exeption)
                        {
                            Console.WriteLine(exeption);
                        }
                        catch
                        {
                            Console.WriteLine("Что-то пошло не так");
                        }
                        break;
                    case "3":
                        Console.WriteLine(workShop.ToString());
                        break;
                    case "4":
                        foreach (var it in workShop.WorkerToString())
                            Console.WriteLine(it);
                        break;
                    case "7":
                        flag = false;
                        break;
                    case "6":
                        try
                        {
                            Console.WriteLine("Введите ID работника");
                            Console.WriteLine($"Заработная плата данного работника равна {workShop.WorkerWage(int.Parse(Console.ReadLine()))}");
                        }
                        catch (ExistExeption exeption)
                        {
                            Console.WriteLine(exeption);
                        }
                        catch
                        {
                            Console.WriteLine("Что-то пошло не так");
                        }
                        break;
                    case "5":
                        Console.WriteLine($"Сумарные затраты на зароботную плату работникам равны { workShop.SumWage()}");
                        break;
                    default: break;
                }
            }
        }
    }
}
