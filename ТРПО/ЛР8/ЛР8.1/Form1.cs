﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using GRASP;

namespace ЛР8._1
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        WorkShop WorkShop;
        private void button1_Click(object sender, EventArgs e)
        {
            WorkShop = new WorkShop(textBox1.Text);
            label1.Visible = false;
            textBox1.Visible = false;
            button1.Visible = false;
            button2.Visible = true; 
            button3.Visible = true; 
            button4.Visible = true; 
            button5.Visible = true; 
            button6.Visible = true; 
            button7.Visible = true; 
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {
            if (textBox1.Text != "")
                button1.Enabled = true;
            else button1.Enabled = false;
        }


        private void textBox3_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;

            if (!Char.IsDigit(number) && number != 8)
            {
                e.Handled = true;
            }
        }

        private void textBox4_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;

            if (!Char.IsDigit(number) && number != 8)
            {
                e.Handled = true;
            }
        }
        int kol1 = 0;

        private void button2_Click(object sender, EventArgs e)
        {
            if (kol1 % 2 == 0)
            {
                textBox2.Visible = true;
                textBox3.Visible = true;
                textBox4.Visible = true;
                label2.Visible = true;
                label3.Visible = true;
                label4.Visible = true;
            }
            else
            {
                if (textBox4.Text != "" && textBox2.Text != "" && textBox3.Text != "") 
                    try
                    {
                        WorkShop.AddWorker(textBox2.Text, int.Parse(textBox3.Text), int.Parse(textBox4.Text));
                        textBox2.Visible = false;
                        textBox3.Visible = false;
                        textBox4.Visible = false;
                        label2.Visible = false;
                        label3.Visible = false;
                        label4.Visible = false;
                    }
                    catch (ExistExeption)
                    {
                        MessageBox.Show("Такой работник уже существует");
                    }
                else MessageBox.Show("Не все поля заполнены");
            }
            kol1++;
        }
        int kol2 = 0;
        private void button3_Click(object sender, EventArgs e)
        {
            if (kol2 % 2 == 0)
            {
                textBox5.Visible = true;
                textBox6.Visible = true;                
                label5.Visible = true;
                label6.Visible = true;
                
            }
            else
            {
                if (textBox5.Text != "" && textBox6.Text != "")
                    try
                    {
                        WorkShop.AddMachine( int.Parse(textBox6.Text), int.Parse(textBox5.Text));
                        textBox5.Visible = false;
                        textBox6.Visible = false;
                        label5.Visible = false;
                        label6.Visible = false;
                        
                    }
                    catch (ExistExeption)
                    {
                        MessageBox.Show("Такой станок на обслуживании у данного работника уже существует");
                    }
                else MessageBox.Show("Не все поля заполнены");
            }
            kol2++;
        }

        private void textBox6_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;

            if (!Char.IsDigit(number) && number != 8)
            {
                e.Handled = true;
            }
        }

        private void textBox5_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;

            if (!Char.IsDigit(number) && number != 8)
            {
                e.Handled = true;
            }
        }

        private void button7_Click(object sender, EventArgs e)
        {
            MessageBox.Show($"Затраты на заработную плату всем сотрудникам - {WorkShop.SumWage()}");
        }

        private void button6_Click(object sender, EventArgs e)
        {
            MessageBox.Show(WorkShop.ToString());
        }

        private void button5_Click(object sender, EventArgs e)
        {
            foreach(var it in WorkShop.WorkerToString())
            MessageBox.Show(it);
        }

        private void textBox7_KeyPress(object sender, KeyPressEventArgs e)
        {
            char number = e.KeyChar;

            if (!Char.IsDigit(number) && number != 8)
            {
                e.Handled = true;
            }
        }
        int kol3 = 0;
        private void button4_Click(object sender, EventArgs e)
        {
            if (kol3 % 2 == 0)
            {
                textBox7.Visible = true;
                label7.Visible = true;

            }
            else
            {
                if (textBox7.Text != "")
                    try
                    {
                        MessageBox.Show($"Заработная плата данного сотрудника равна - {WorkShop.WorkerWage(int.Parse(textBox7.Text))}");
                        textBox7.Visible = false;
                        label7.Visible = false;

                    }
                    catch (ExistExeption)
                    {
                        MessageBox.Show("Такого рабочего нет");
                    }
                else MessageBox.Show("Поле не заполнены");
            }
            kol3++;
        }
    }
}
