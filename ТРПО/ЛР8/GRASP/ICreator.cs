﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRASP
{
    interface ICreator
    {
        void AddWorker(string FIO, int id, int id_machinr);
        void AddMachine(int id_worker, int id);
    }
}
