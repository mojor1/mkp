﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRASP
{
    public class WorkShop: ICreator
    {
        string adress;
        List<Worker> workers = new List<Worker>();

        public WorkShop(string adress)
        {
            this.adress = adress;
        }

        public WorkShop()
        {
        }

        public void AddWorker(string FIO, int id, int id_machine)
        {
            bool flag = false;
            foreach (var it in workers)
                if (it.id == id)
                    flag= true;
            if (!flag)
            {
                workers.Add(new Worker(FIO, id));
                AddMachine(id, id_machine);
            }
            else throw new ExistExeption("Работник с таким номером уже существует!");
        }
        public double SumWage()
        {
            double a = 0;
            foreach (var it in workers)
                a += it.Wage();
            return a;
        }
        public void AddMachine(int id_worker, int id)
        {
            bool flag = false;
            foreach (var it in workers)
                if (it.id == id_worker)
                {
                    it.AddMachine(id);
                    flag = true;
                }
            if (!flag) throw new ExistExeption("Такого id работника нет!");
        }
        public override string ToString()
        {
            return $"Адресс - {adress}, Количество работников - {workers.Count}";
        }
        public string[] WorkerToString()
        {
            string[] a = new string[workers.Count];
            int i = 0;
            foreach(var it in workers)
            {
                a[i] = it.ToString();
                i++;
            }
            return a;
        }
        public double WorkerWage(int id)
        {
            double wage = 0;
            bool flag = false;
            foreach (var it in workers)
                if (it.id == id)
                {
                    wage= it.Wage();
                    flag = true;
                }
            if (!flag) throw new ExistExeption("Такого id работника нет!");
            return wage;
        }
    }
}
