﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRASP
{
    public class machine
    {
        public int id;

        public machine(int id)
        {
            this.id = id;
        }
        public override string ToString()
        {
            return $"ID станка - {id}";
        }
    }
}
