﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GRASP
{
    public class Worker
    {
        public int id;
        public string FIO;
        const double wage=700;
        const double premium=100;
        List<machine> machines = new List<machine>();

        public Worker(string FIO, int id)
        {
            this.FIO = FIO;
            this.id = id;
        }
       public void AddMachine(int id)
       {
            bool flag = false;
                foreach (var it in machines)
                    if (it.id == id)
                        flag = true;
            if (!flag)
                machines.Add(new machine(id));
             else throw new ExistExeption("Станок с таким номером уже существует!");
        }
        public double Wage()
        {
            return wage + (machines.Count - 1) * premium;
        }
        public override string ToString()
        {
            return $"ID работника - {id}, ФИО работника - {FIO}, Количество станков - {machines.Count}, Зарплата - {Wage()} Станки:\n {MachineToString()}";
        }
        public string MachineToString()
        {
            string a = "";
            foreach (var it in machines)
                a += it.ToString() + ' ';
            return a;
        }
    
    }
}
