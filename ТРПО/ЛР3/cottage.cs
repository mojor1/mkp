﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР3
{
    class cottage :Dacha, ICottage
    {
       public int kol_etageq { get; set; }
        public cottage(int kol_komnat, int kol_etageq) : base(kol_komnat)
        {
            this.kol_etageq = kol_etageq;
        }
        public void Get_kol_etageq()
        {
            Console.WriteLine("Количество этажей равно "+kol_etageq);
        }
        public override void Result()
        {
            Console.WriteLine("Коттедж построен");
        }
    }
}
