﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР3
{
    class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("1 Построить дачу"); Console.WriteLine("2 Построить коттедж"); Console.WriteLine("3 Построить небоскрёб");
            string a = Console.ReadLine();
            switch (a)
            {
                case "1":
                    Console.WriteLine("Укажите количество комнат");
                    Dacha dacha = new Dacha(int.Parse(Console.ReadLine()));
                    dacha.Get_kol_komnat();
                    dacha.Result();
                    break;
                case "2":
                    Console.WriteLine("Укажите количество комнат и количество этажей соответственно через ентер");
                    cottage cottage = new cottage(int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()));
                    cottage.Get_kol_etageq();
                    cottage.Get_kol_komnat();
                    cottage.Result();
                    break;
                case "3":
                    Console.WriteLine("Укажите количество комнат, количество этажей и количество квартир соответственно через ентер");
                    Skyscraper skyscraper = new Skyscraper(int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()), int.Parse(Console.ReadLine()));
                    skyscraper.Get_kol_flats();
                    skyscraper.Get_kol_etageq();
                    skyscraper.Get_kol_komnat();
                    skyscraper.Result();
                    break;
                default: break;
            }
            Console.ReadKey();

        }
    }
}
