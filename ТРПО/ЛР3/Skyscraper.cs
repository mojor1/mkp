﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР3
{
    class Skyscraper:cottage, ISkyscraper
    {
        public int kol_flats { get; set; }
        public Skyscraper(int kol_komnat, int kol_etageq, int kol_flats) : base(kol_komnat, kol_etageq) {
            this.kol_flats = kol_flats;
        }
        public void Get_kol_flats()
        {
            Console.WriteLine("Количество квартир равно " + kol_flats);
        }
        public override void Result()
        {
            Console.WriteLine("Небоскрёб построен");
        }
    }
}
