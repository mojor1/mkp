﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР6
{
    class Gas
    {
       public double Volume;
      public  double mass;
       public double molear;
        public Gas(double dV, double mas, double molear)
        {
            Volume = dV;
            this.mass = mas;
            this.molear = molear;
        }
        public  double GetPressure(int T)
        {
            double P=mass*8.31*T/(molear * Volume);
            return P;
        }
      public  double AmountOfMatter()
        {
            return mass / molear;
        }
        public  override string ToString()
        {
            return $"объём равен  {Volume}, масса равна {mass}, молярная масса равна {molear}";
        }

    }
}
