﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР6
{
    class Balon:IGas
    {
        public Gas gas;
        public Balon(double dV, double mas, double molear)
        {
            gas = new Gas(dV, mas, molear);
        }
        public void ModifVolume(double dV)
        {
            gas.Volume += dV;
        }
       public double GetDp(int T0, int T1)
        {

            return gas.GetPressure(T1) - gas.GetPressure(T0);
        }
        public string Pasport()
        {
            return gas.ToString();
        }
    }
}
