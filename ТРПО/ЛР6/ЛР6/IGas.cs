﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР6
{
    interface IGas
    {
        void ModifVolume(double dV);
        double GetDp(int T0, int T1);
        string Pasport();
    }
}
