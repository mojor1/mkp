﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ЛР5
{
    class Program
    {
        public interface ICinima
        {
            IFilm Film(string filmname);
        }

        // Конкретная Фабрика производит семейство продуктов одной вариации. Фабрика
        // гарантирует совместимость полученных продуктов.  Обратите внимание, что
        // сигнатуры методов Конкретной Фабрики возвращают абстрактный продукт, в то
        // время как внутри метода создается экземпляр  конкретного продукта.
        class Cinima : ICinima
        {
            public IFilm Film(string filmname)
            {
                return new Film(filmname);
            }
        }

        // Каждая Конкретная Фабрика имеет соответствующую вариацию продукта.

        // Каждый отдельный продукт семейства продуктов должен иметь базовый
        // интерфейс. Все вариации продукта должны реализовывать этот интерфейс.
        public interface IFilm
        {
            void watchFilm();
            ISubtitels subtitelsRus();
            ISubtitels subtitelsEng();
        }

        // Конкретные продукты создаются соответствующими Конкретными Фабриками.
        class Film : IFilm
        {
            string Filmname { get; set; }
            public Film(string filmname)
            {
                Filmname = filmname;
            }
            public void watchFilm()
            {
                Console.WriteLine($"Вы смотрите фильм {Filmname}");
            }
            public ISubtitels subtitelsRus()
            {
                return new SubtitelsRus();
            }
            public ISubtitels subtitelsEng()
            {
                return new SubtitelsEng();
            }
        }

        // Базовый интерфейс другого продукта. Все продукты могут взаимодействовать
        // друг с другом, но правильное взаимодействие возможно только между
        // продуктами одной и той же конкретной вариации.
        public interface ISubtitels
        {
            // Продукт B способен работать самостоятельно...
            void subtitels();

            // ...а также взаимодействовать с Продуктами А той же вариации.
            //
            // Абстрактная Фабрика гарантирует, что все продукты, которые она
            // создает, имеют одинаковую вариацию и, следовательно, совместимы.
           
        }
         public class SubtitelsRus : ISubtitels
        {
            public void subtitels()
            {
                Console.WriteLine("Субтитры и звуковая дорожка на русском");
            }

        }
        public class SubtitelsEng : ISubtitels
        {
            public void subtitels()
            {
                Console.WriteLine("Субтитры и звуковая дорожка на английском");
            }

        }
        // Конкретные Продукты создаются соответствующими Конкретными Фабриками.


        // Клиентский код работает с фабриками и продуктами только через абстрактные
        // типы: Абстрактная Фабрика и Абстрактный Продукт. Это позволяет передавать
        // любой подкласс фабрики или продукта клиентскому коду, не нарушая его.
        class Client
        {
            public void Main()
            {
                // Клиентский код может работать с любым конкретным классом фабрики.
                Console.WriteLine("Введите название фильма");
                string a = Console.ReadLine();
                ClientMethod(new Cinima(), a);
            }

            public void ClientMethod(ICinima factory, string filmname)
            {
                var film = factory.Film(filmname);
                Console.WriteLine("Если хотите субтитры на руском, то введите цифру 1, если хотите субтитры на английскром, то введите цифру 2");
                string b = Console.ReadLine();
                if (b == "1")
                   film.subtitelsRus().subtitels();
                    else 
                        if (b == "2")
                        film.subtitelsEng().subtitels();
                        else Console.WriteLine("Нет такого номера");
                
            }
        }

        
            static void Main(string[] args)
            {
                new Client().Main();
            }
    }
}

