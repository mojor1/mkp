﻿using System;
using System.Collections.Generic;
using System.Text;
using System.IO;
using System.Net.NetworkInformation;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Data.SqlTypes;
using System.Dynamic;

namespace ConsoleApp2
{
    class file2 :file1
    {
        public file2(string filename) : base(filename)
        {

        }

        public List<data> GetData()
        {
            List<data> Datas = new List<data>();
            string s = File.ReadAllText(Filename);
            string[] s1 = s.Split();
            for (int i = 0; i < s1.Length; i++)
            {

                data data = new data(s1[i]);
                Datas.Add(data);

            }
            return Datas;

        }
    }
}
